<?php

namespace app\modules\api\controllers;

use app\models\Report;
use yii\web\Controller;

/**
 * Default controller for the `api` module
 */
class CronController extends Controller
{
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Проверяет отправили ли клиенты отчеты по счетчикам.
     * Если по какому-то (каким-то) аппарату не отправлен отчет, формируется письмо и отправляется клиенту
     */
    public function actionCheckReports()
    {
        try {
            return Report::checkReports();
        } catch (\HttpInvalidParamException $e) {
            \Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        }
    }

}
