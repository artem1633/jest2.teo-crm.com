<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%complaint}}`.
 */
class m200528_075233_create_complaint_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%complaint}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заявка'),
            'header' => $this->string()->comment('Заголовок'),
            'text' => $this->text()->comment('Текст'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);

        $this->addCommentOnTable('{{%complaint}}', 'Жалобы');

        $this->addForeignKey(
            'fk-complaint-order_id',
            '{{%complaint}}',
            'order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%complaint}}');
    }
}
