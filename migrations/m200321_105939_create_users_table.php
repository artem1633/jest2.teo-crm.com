<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%users}}`.
 */
class m200321_105939_create_users_table extends Migration
{
     public function safeUp()
    {
        $this->createTable('users', [
            'id' => $this->primaryKey(),
            'fio' => $this->string()->comment('ФИО'),
            'role' => $this->integer()->defaultValue(2)->comment("Роль"),
            'login' => $this->string(255)->comment("Логин"),
            'password' => $this->string(255)->comment("Пароль"),
            'password_reset_token' => $this->string(255),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'client_id' => $this->integer()->comment('Клиент'),
            'avatar' => $this->string()->comment('Путь к аватару')
        ]);

        $this->insert('users',array(
            'fio' => 'Иванов Иван Иванович',
            'role' => 1,
            'login' => 'admin',
            'password' => Yii::$app->security->generatePasswordHash('admin'),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('users');
    }
}
