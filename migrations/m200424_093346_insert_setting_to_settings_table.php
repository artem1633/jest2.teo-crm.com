<?php

use yii\db\Migration;

/**
 * Class m200424_093346_insert_setting_to_settings_table
 */
class m200424_093346_insert_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            'key' => 'report_recipient',
            'value' => 'jest.cms.2020@yandex.ru',
            'label'=> 'Получатели писем с показаниями счетчиков (адреса через запятую)'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200424_093346_insert_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200424_093346_insert_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
