<?php

use yii\db\Migration;

/**
 * Class m200528_074115_add_foreigin_keys_to_order_table
 */
class m200528_074115_add_foreigin_keys_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-order-apparatus_client_id',
            '{{%order}}',
            'apparatus_client_id',
            '{{%apparatus_to_client}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-parent_order_id',
            '{{%order}}',
            'parent_order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-manager_id',
            '{{%order}}',
            'manager_id',
            '{{%users}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-engineer_id',
            '{{%order}}',
            'engineer_id',
            '{{%users}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200528_074115_add_foreigin_keys_to_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200528_074115_add_foreigin_keys_to_order_table cannot be reverted.\n";

        return false;
    }
    */
}
