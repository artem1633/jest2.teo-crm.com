<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%condition}}`.
 */
class m200528_083816_create_condition_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%condition}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заявка'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'description' => $this->text()->comment('Описание'),
        ]);

        $this->addCommentOnTable('{{%condition}}', 'Состояние');

        $this->addForeignKey(
            'fk-condition-order_id',
            '{{%condition}}',
            'order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%condition}}');
    }
}
