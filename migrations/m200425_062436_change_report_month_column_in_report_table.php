<?php

use yii\db\Migration;

/**
 * Class m200425_062436_change_report_month_column_in_report_table
 */
class m200425_062436_change_report_month_column_in_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%report}}', 'report_month', $this->smallInteger()->comment('Отчетный месяц'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200425_062436_change_report_month_column_in_report_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200425_062436_change_report_month_column_in_report_table cannot be reverted.\n";

        return false;
    }
    */
}
