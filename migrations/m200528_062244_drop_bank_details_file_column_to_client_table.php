<?php

use yii\db\Migration;

/**
 * Handles the dropping of table `{{%bank_details_file_column_to_client}}`.
 */
class m200528_062244_drop_bank_details_file_column_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropColumn('{{%client}}', 'bank_details_file');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200528_062244_drop_bank_details_file_column_to_client_table cannot be reverted.\n";

        return false;
    }
}
