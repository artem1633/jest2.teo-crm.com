<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work}}`.
 */
class m200528_080925_create_work_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заявка'),
            'status' => $this->integer()->comment('Статус Выполненные/Необходимые'),
            'content' => $this->text()->comment('Содержание работ'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);

        $this->addCommentOnTable('{{%work}}', 'Работы');

        $this->addForeignKey(
            'fk-work-order_id',
            '{{%work}}',
            'order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work}}');
    }
}
