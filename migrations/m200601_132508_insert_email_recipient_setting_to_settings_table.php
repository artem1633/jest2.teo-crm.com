<?php

use yii\db\Migration;

/**
 * Class m200601_132508_insert_email_recipient_setting_to_settings_table
 */
class m200601_132508_insert_email_recipient_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', [
            'key' => 'repeat_order_notify_emails',
            'value' => 'dir@gest.ru',
            'label' => 'Адрес почты для уведомления о повторной заявке'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200601_132508_insert_email_recipient_setting_to_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200601_132508_insert_email_recipient_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
