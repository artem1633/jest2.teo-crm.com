<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%report}}`.
 */
class m200423_124342_create_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%report}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'sented_at' => $this->timestamp()->defaultValue(null)->comment('Дата отправки'),
            'apparatus_to_client_id' => $this->integer(),
            'counter_1' => $this->integer()->comment('Показания счетчика 1'),
            'counter_2' => $this->integer()->comment('Показания счетчика 2'),
            'photo' => $this->text()->comment('Фото счетчика/ов'),
            'report_month' => $this->timestamp()->defaultValue(null),
            'status' => $this->smallInteger()->defaultValue(1)->comment('Статус отчета'),
            'comment' => $this->text()->comment('Комментарий к отчету'),
        ]);

        $this->addForeignKey(
            'fk-report-apparatus_to_client_id',
            '{{%report}}',
            'apparatus_to_client_id',
            'apparatus_to_client',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%report}}');
    }
}
