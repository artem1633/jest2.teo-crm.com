<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%contract}}`.
 */
class m200528_062052_add_file_column_to_contract_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%contract}}', 'file', $this->text()->comment('Путь к файлу контракта'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%contract}}', 'file');
    }
}
