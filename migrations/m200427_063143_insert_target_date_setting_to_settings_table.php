<?php

use yii\db\Migration;

/**
 * Class m200427_063143_insert_target_date_setting_to_settings_table
 */
class m200427_063143_insert_target_date_setting_to_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%settings}}', ['key' => 'report_day', 'value' => 20, 'label' => 'Отчетный день месяца']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200427_063143_insert_target_date_setting_to_settings_table cannot be reverted.\n";

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200427_063143_insert_target_date_setting_to_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
