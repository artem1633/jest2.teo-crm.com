<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order}}`.
 */
class m200414_084534_create_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'client_id' => $this->integer()->comment('Клиент'),
            'apparatus_id' => $this->integer()->comment('Аппарат'),
            'counter_1' => $this->integer()->comment('Счетчик 1'),
            'counter_2' => $this->integer()->comment('Счетчик 2'),
            'counter_3' => $this->integer()->comment('Счетчик 3'),
            'photos' => $this->string()->comment('Путь к папке с фото счетчиков'),
            'done_at' => $this->timestamp()->defaultValue(null)->comment('Дата закрытия заявки'),
        ]);

        $this->addForeignKey(
            'fk-order-client_id',
            '{{%order}}',
            'client_id',
            'client',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order-apparatus_id',
            '{{%order}}',
            'apparatus_id',
            'apparatus',
            'id',
            'SET NULL', //При удалении аппарата заявку не удаляем
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order}}');
    }
}
