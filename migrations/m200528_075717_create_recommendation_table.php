<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%recommendation}}`.
 */
class m200528_075717_create_recommendation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%recommendation}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заявка'),
            'header' => $this->string()->comment('Заголовок'),
            'text' => $this->text()->comment('Текст'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
        ]);

        $this->addCommentOnTable('{{%recommendation}}', 'Рекомендации');

        $this->addForeignKey(
            'fk-recommendation-order_id',
            '{{%recommendation}}',
            'order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%recommendation}}');
    }
}
