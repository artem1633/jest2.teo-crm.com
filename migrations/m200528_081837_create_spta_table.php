<?php

use yii\db\Migration;

/**
 * spare parts, tools and accessories по нашему ЗИП
 * Handles the creation of table `{{%spta}}`.
 */
class m200528_081837_create_spta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%spta}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заявка'),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'type' => $this->integer()->comment('Тип Установленные/Необходимые'),
            'name' => $this->string()->comment('Название'),
        ]);

        $this->addCommentOnTable('{{%spta}}', 'ЗИП');

        $this->addForeignKey(
            'fk-spta-order_id',
            '{{%spta}}',
            'order_id',
            '{{%order}}',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%spta}}');
    }
}
