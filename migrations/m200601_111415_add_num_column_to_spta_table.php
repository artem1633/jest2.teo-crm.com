<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%spta}}`.
 */
class m200601_111415_add_num_column_to_spta_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%spta}}', 'num', $this->double(2)->comment('Кол-во'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
       $this->dropColumn('{{%spta}}', 'num');
    }
}
