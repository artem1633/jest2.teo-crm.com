<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%consumables}}`.
 */
class m200414_074226_create_consumables_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%consumables}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->comment('Наименование расходника'),
            'apparatus_type' => $this->smallInteger()->defaultValue(1)
                ->comment('Тип аппарата для которого предназначен расходник')
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%consumables}}');
    }
}
