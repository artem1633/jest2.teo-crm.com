<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apparatus}}`.
 */
class m200414_073523_create_apparatus_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apparatus}}', [
            'id' => $this->primaryKey(),
            'brand_id' => $this->integer()->comment('Марка'),
            'model' => $this->string()->comment('Модель'),
            'type' => $this->smallInteger()->defaultValue(1)->comment('Тип принтера (1-Монохром, 2-Цветной)')
        ]);

        $this->addForeignKey(
            'fk-apparatus-brand_id',
            '{{%apparatus}}',
            'brand_id',
            'brand',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apparatus}}');
    }
}
