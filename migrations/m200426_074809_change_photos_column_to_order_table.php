<?php

use yii\db\Migration;

/**
 * Class m200426_074809_change_photos_column_to_order_table
 */
class m200426_074809_change_photos_column_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'photos', $this->string()->comment('Путь к фотографии счетчиков'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200426_074809_change_photos_column_to_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200426_074809_change_photos_column_to_order_table cannot be reverted.\n";

        return false;
    }
    */
}
