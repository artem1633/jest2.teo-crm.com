<?php

use yii\db\Migration;

/**
 * Class m200416_193022_change_field_type_to_brand_tables
 */
class m200416_193022_change_field_type_to_brand_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%brand}}', 'name', $this->string()->comment('Наименование'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200416_193022_change_field_type_to_brand_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200416_193022_change_field_type_to_brand_tables cannot be reverted.\n";

        return false;
    }
    */
}
