<?php

use yii\db\Migration;

/**
 * Class m200528_084919_clean_settings_table
 */
class m200528_084919_clean_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->delete('{{%settings}}');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200528_084919_clean_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200528_084919_clean_settings_table cannot be reverted.\n";

        return false;
    }
    */
}
