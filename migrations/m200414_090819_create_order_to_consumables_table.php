<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_to_consumables}}`.
 */
class m200414_090819_create_order_to_consumables_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_to_consumables}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->comment('Заказ'),
            'consumables_id' => $this->integer()->comment('Расходники'),
            'number' => $this->double()->comment('Кол-во')
        ]);

        $this->addForeignKey(
            'fk-order_to_consumables-order_id',
            '{{%order_to_consumables}}',
            'order_id',
            'order',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-order_to_consumables-consumables_id',
            '{{%order_to_consumables}}',
            'consumables_id',
            'consumables',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_to_consumables}}');
    }
}
