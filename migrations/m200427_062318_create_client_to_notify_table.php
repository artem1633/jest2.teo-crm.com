<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%client_to_notify}}`.
 */
class m200427_062318_create_client_to_notify_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%client_to_notify}}', [
            'id' => $this->primaryKey(),
            'client_id' => $this->integer(),
            'notify_date' => $this->timestamp()->defaultExpression('NOW()')->comment('Дата отправки напоминания о необходимости отчета')
        ]);

        $this->addForeignKey(
            'fk-client_to_notify-client_id',
            '{{%client_to_notify}}',
            'client_id',
            '{{%client}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%client_to_notify}}');
    }
}
