<?php

use yii\db\Migration;

/**
 * Class m200528_062902_change_order_table
 */
class m200528_062902_change_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropForeignKey('fk-order-apparatus_id', '{{%order}}');
        $this->dropColumn('{{%order}}', 'apparatus_id');


        $this->addCommentOnColumn('{{%order}}', 'photos', 'Путь к папке с фотками счетчиков');

        $this->addColumn('{{%order}}', 'apparatus_client_id', $this->integer()->comment('Аппарат клиента'));
        $this->addColumn('{{%order}}', 'service_type', $this->integer()->comment('Тип услуг (Выезд/Стационар)'));
        $this->addColumn('{{%order}}', 'work_type', $this->integer()->comment('Тип работ (Гарантия/Подготовка/Платный/Контракт)'));
        $this->addColumn('{{%order}}', 'parent_order_id', $this->integer()->comment('Родительская заявка'));
        $this->addColumn('{{%order}}', 'total_time', $this->integer()->comment('Затраченное время (час.)'));
        $this->addColumn('{{%order}}', 'account_number', $this->string()->comment('Номер счета'));
        $this->addColumn('{{%order}}', 'manager_id', $this->integer()->comment('Диспетчер'));
        $this->addColumn('{{%order}}', 'engineer_id', $this->integer()->comment('Инженер'));

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200528_062902_change_order_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200528_062902_change_order_table cannot be reverted.\n";

        return false;
    }
    */
}
