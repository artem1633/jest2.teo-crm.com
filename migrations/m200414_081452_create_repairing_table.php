<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%repairing}}`.
 */
class m200414_081452_create_repairing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%repairing}}', [
            'id' => $this->primaryKey(),
            'created_at' => $this->timestamp()->defaultExpression('NOW()'),
            'client_id' => $this->integer()->comment('Клиент'),
            'apparatus_id' => $this->integer()->comment('Аппарат'),
            'counter_1' => $this->integer()->comment('Счетчик 1'),
            'counter_2' => $this->integer()->comment('Счетчик 2'),
            'counter_3' => $this->integer()->comment('Счетчик 3'),
            'description' => $this->text()->comment('Описание инцидента'),
            'photos' => $this->string()->comment('Путь к папке с фото'),
            'done_at' => $this->timestamp()->defaultValue(null)->comment('Дата закрытия заявки'),
        ]);

        $this->addForeignKey(
            'fk-repairing-client_id',
            '{{%repairing}}',
            'client_id',
            'client',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-repairing-apparatus_id',
            '{{%repairing}}',
            'apparatus_id',
            'apparatus',
            'id',
            'CASCADE',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%repairing}}');
    }
}
