<?php

use yii\db\Migration;

/**
 * Class m200415_074121_insert_data_to_tables
 */
class m200415_074121_insert_data_to_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%consumables}}', ['name', 'apparatus_type'],[
           ['Тонер', 1],
           ['Барабан', 1],
           ['Тонер Голубой', 2],
           ['Тонер Малиновый', 2],
           ['Тонер Желтый', 2],
           ['Тонер Черный', 2],
           ['Барабан Голубой', 2],
           ['Барабан Малиновый', 2],
           ['Барабан Желтый', 2],
           ['Барабан Черный', 2],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200415_074121_insert_data_to_tables cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200415_074121_insert_data_to_tables cannot be reverted.\n";

        return false;
    }
    */
}
