<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%apparatus_to_client}}`.
 */
class m200414_073732_create_apparatus_to_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%apparatus_to_client}}', [
            'id' => $this->primaryKey(),
            'apparatus_id' => $this->integer()->comment('Аппарат'),
            'client_id' => $this->integer()->comment('Клиент'),
            'notes' => $this->text()->comment('Примечание'),
            'is_owner' => $this->smallInteger()->defaultValue(1)->comment('Является ли клиент собственником аппарата'),
            'serial_number' => $this->string()->comment('Серийный номер аппарата'),
        ]);

        $this->addForeignKey(
            'fk-apparatus_to_client-apparatus_id',
            '{{%apparatus_to_client}}',
            'apparatus_id',
            '{{%apparatus}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-apparatus_to_client-client_id',
            '{{%apparatus_to_client}}',
            'client_id',
            '{{%client}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%apparatus_to_client}}');
    }
}
