<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%settings}}`.
 */
class m200419_103025_create_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%settings}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(),
            'value' => $this->string(),
            'label' => $this->string()
        ]);

        $this->batchInsert('{{%settings}}', ['key', 'value', 'label'],[
            ['order_recipient', 'jest.cms.2020@mail.ru', 'Получатели писем о заказе расходников (адреса через запятую)'],
            ['repairing_recipient', 'jest.cms.2020@mail.ru', 'Получатели писем об инциденте (адреса через запятую)'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%settings}}');
    }
}
