<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%report}}`.
 */
class m200425_062330_add_report_year_column_to_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%report}}', 'report_year', $this->integer()->comment('Отчетный год'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%report}}', 'report_year');
    }
}
