<?php

/* @var $this yii\web\View */
/* @var $model app\models\Complaint */
?>
<div class="complaint-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
