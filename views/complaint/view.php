<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Complaint */
?>
<div class="complaint-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'order_id',
                'header',
                'text:ntext',
                'created_at',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
