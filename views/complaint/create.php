<?php

/* @var $this yii\web\View */
/* @var $model app\models\Complaint */

?>
<div class="complaint-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
