<?php

use app\models\Complaint;
use yii\helpers\Html;

/** @var \app\models\Order $model */


$counter = 1;
?>

<div class="row">
    <?php /** @var Complaint $complaint */
    foreach ($model->complaints as $complaint):?>
        <div class="col-sm-4">
            <div class="well" style="position: relative">
                <div class="complaint-text">
                    <?= 'Жалоба ' . $counter . '.<br>' . $complaint->text; ?>
                </div>
                <?= Html::a('<span class="fa fa-edit edit-complaint"></span>',
                    ['/complaint/update', 'id' => $complaint->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 2rem; font-size: 1.5rem'
                    ]) ?>
                <?= Html::a('<span class="fa fa-close delete-complaint"></span>',
                    ['/complaint/delete', 'id' => $complaint->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 0.5rem; font-size: 1.5rem; color: red;',
                        'title' => 'Удалить жалобу',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите удаление жалобы',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',
                    ]) ?>
            </div>
        </div>
        <?php $counter++; ?>
    <?php endforeach; ?>
</div>
