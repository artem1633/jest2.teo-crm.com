<?php

use app\models\constants\UsersConstants;
use app\models\Order;
use app\models\Users;
use yii\helpers\Html;
use app\widgets\Menu;

$pathInfo = Yii::$app->request->pathInfo;

/** @var Users $identity */
$identity = Yii::$app->user->identity ?? null;

$current_order_label = (new Order())->getLabelCounts();
$repairing_label = (new \app\models\Repairing())->getLabelCounts();

$request = Yii::$app->request;

$is_repeat_order = isset($request->get('OrderSearch')['is_repeat']) && $request->get('OrderSearch')['is_repeat'] == 1;
$is_current_order = isset($request->get('OrderSearch')['status']) && $request->get('OrderSearch')['status'] == Order::STATUS_CURRENT;
$is_done_order = isset($request->get('OrderSearch')['status']) && $request->get('OrderSearch')['status'] == Order::STATUS_DONE;

//Yii::warning(Yii::$app->controller->action->id, 'test');
?>

<div id="sidebar" class="sidebar sidebar-transparent">
    <!-- begin sidebar scrollbar -->
    <div data-scrollbar="true" data-height="100%">
        <!-- begin sidebar user -->

        <ul class="nav">
            <?php if (isset(Yii::$app->user->identity)) { ?>
                <li class="nav-profile">
                    <a href="javascript:" data-toggle="nav-profile">
                        <div class="cover with-shadow"></div>
                        <div class="image">
                            <img src="<?= $identity->getAvatar() ?>" alt=""/>
                        </div>
                        <div class="info">
                            <b class="caret pull-right"></b>
                            <?= $identity->fio ?>
                            <small><?= $identity->getTypeDescription() ?></small>
                        </div>
                    </a>
                </li>
            <?php } ?>
            <li>
                <ul class="nav nav-profile">
                    <li><?= Html::a('<i class="fa fa-cogs"></i> Изменить Профиль', ['/users/profile'], []); ?></li>
                    <li><?= Html::a(
                            '<i class="fa fa-sign-out"></i>Выйти',
                            ['/site/logout'],
                            ['data-method' => 'post',]
                        ) ?></li>
                </ul>
            </li>
        </ul>
        <!-- end sidebar user -->
        <!-- begin sidebar nav -->

        <?php
        try {
            echo Menu::widget(
                [
                    'options' => ['class' => 'nav'],
                    'items' => [
//                        [
//                            'label' => 'Инциденты' . $repairing_label,
//                            'encode' => false,
//                            'icon' => 'wrench',
//                            'url' => ['/repairing',],
//                            'active' => $this->context->id == 'repairing'
//                        ],
                        [
                            'label' => 'Создание новой заявки',
                            'icon' => 'plus',
                            'encode' => false,
                            'url' => ['/order/create',],
                        ],

                        [
                            'label' => 'Справочники',
                            'icon' => 'book',
                            'url' => ['#'],
                            'visible' => UsersConstants::isAdministration(),
                            'items' => [
                                [
                                    'label' => 'Клиенты',
                                    'icon' => 'users',
                                    'url' => ['/client'],
                                    'active' => $this->context->id == 'client',
                                    'visible' => $identity ? (in_array($identity->role, [1, 2]) ? true : false) : false,
                                ],
                                [
                                    'label' => 'Аппараты',
                                    'icon' => 'print',
                                    'url' => ['/apparatus',],
                                    'active' => $this->context->id == 'apparatus'
                                ],
                                [
                                    'label' => 'Инженеры',
                                    'icon' => 'users',
                                    'url' => ['/users/engineer'],
//                                    'active' => $this->context->id == 'users'
                                    'active' => Yii::$app->controller->action->id == 'engineer'
                                ],
                                [
                                    'label' => 'Диспетчеры',
                                    'icon' => 'users',
                                    'url' => ['/users/manager'],
                                    'active' => Yii::$app->controller->action->id == 'manager'
                                ],

                            ],
                        ],
                        [
                            'label' => 'Заявки',
                            'icon' => 'list',
                            'url' => ['#'],
                            'visible' => UsersConstants::isAdministration(),
                            'items' => [
                                [
                                    'label' => 'Текущие ' .  $current_order_label,
                                    'encode' => false,
                                    'icon' => 'users',
                                    'url' => ['/order', 'OrderSearch[status]' => Order::STATUS_CURRENT, 'append' => 'Текущие'],
                                    'active' => $is_current_order,
                                ],
                                [
                                    'label' => 'Закрытые',
                                    'icon' => 'print',
                                    'url' => ['/order', 'OrderSearch[status]' => Order::STATUS_DONE, 'append' => 'Закрытые'],
                                    'active' => $is_done_order,
                                ],
                                [
                                    'label' => 'Повторные',
                                    'icon' => 'users',
                                    'url' => ['/order', 'OrderSearch[is_repeat]' => 1, 'append' => 'Повторные'],
                                    'active' => $is_repeat_order,
                                ],

                            ],
                        ],
                        [
                            'label' => 'Информация по аппаратам',
                            'icon' => 'print',
                            'url' => ['/apparatus-to-client/info'],
                            'active' => $this->context->id == 'settings',
                            'visible' => UsersConstants::isAdministration(),
                        ],
                        [
                            'label' => 'Настройки',
                            'icon' => 'cog',
                            'url' => ['/settings'],
                            'active' => $this->context->id == 'settings',
                            'visible' => UsersConstants::isSuperAdmin(),
                        ],
//                        [
//                            'label' => 'Помощь',
//                            'icon' => 'question-circle',
//                            'url' => ['/site/help'],
//                            'active' => $this->context->id == 'settings',
////                            'visible' => !UsersConstants::isSuperAdmin(),
//                        ],
                    ],
                ]
            );
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>
        <li style="list-style: none;"><a href="javascript:" class="sidebar-minify-btn" data-click="sidebar-minify"><i
                        class="fa fa-angle-double-left"></i></a></li>
        <!-- end sidebar nav -->
    </div>
    <!-- end sidebar scrollbar -->
</div>
<div class="sidebar-bg"></div>