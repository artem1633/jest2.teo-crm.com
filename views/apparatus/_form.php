<?php

use app\models\Brand;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Apparatus */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="apparatus-form">

        <?php $form = ActiveForm::begin(); ?>

        <div class="row">
            <div class="col-md-10">
                <div id="brand-select">
                    <?php try {
                        echo $form->field($model, 'brand_id')->widget(Select2::class, [
                            'data' => (new Brand)->list,
                            'options' => [
                                'placeholder' => 'Выберите марку'
                            ]
                        ]);
                    } catch (Exception $e) {
                        Yii::error($e->getMessage(), '_error');
                        echo $e->getMessage();
                    } ?>
                </div>
                <div id="brand-input" style="display:none;">
                    <?= $form->field($model, 'brand_string')->textInput(); ?>
                </div>

            </div>
            <div class="col-md-2">
                <div class="switch-button" style="height: 7.5rem; align-items: center; display: flex;">
                    <?= Html::button('<i class="fa fa-pencil"></i>', [
                        'id' => 'switch-button',
                        'data-action' => 'manual-edit',
                        'class' => 'btn btn-primary btn-block',
                        'title' => 'Ввести марку вручную',
                    ]) ?>
                </div>
            </div>
        </div>

        <?php //echo  $form->field($model, 'brand_id')->textInput() ?>

        <?= $form->field($model, 'model')->textInput(['maxlength' => true]) ?>

        <?php try {
            echo $form->field($model, 'type')->widget(Select2::class, [
                'data' => $model->typeList,
                'options' => [
                    'placeholder' => 'Выберите тип аппарата'
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
var brand_select = $('#brand-select');
var brand_input = $('#brand-input');
var switch_btn = $('#switch-button');

$(document).on('click', '[data-action="manual-edit"]', function() {
      brand_select.hide();
      brand_select.find('select').attr('selectedIndex', 0);
      brand_input.show();
      switch_btn.attr('data-action', 'select');
      switch_btn.attr('title', 'Выбрать из списка');
      switch_btn.find('i').toggleClass('fa-pencil').toggleClass('fa-reorder');
      return true;
});
$(document).on('click', '[data-action="select"]', function() {
      brand_select.show();
      brand_input.hide();
      brand_input.find('input').val('');
      switch_btn.attr('data-action', 'manual-edit');
      switch_btn.attr('title', 'Ввести вручную');
      switch_btn.find('i').toggleClass('fa-reorder').toggleClass('fa-pencil');
      return true;
});
JS;

$this->registerJs($script);