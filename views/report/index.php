<?php

use app\models\constants\UsersConstants;
use johnitvn\ajaxcrud\BulkButtonWidget;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset; 

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Ежемесячные показания счетчиков';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse report-index">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Показания счетчиков</h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered table-hover'],
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_columns.php'),
                    'responsiveWrap' => false,
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
                                    [
                                        'role' => 'modal-remote',
                                        'title' => 'Создать отчет',
                                        'class' => UsersConstants::isSuperAdmin() ? 'hidden' : 'btn btn-default',
                                    ]) .
                                '{toggleData}' .
                                '{export}'
                        ],
                    ],
                    'panel' => [
                        'after' => UsersConstants::isSuperAdmin() ?  BulkButtonWidget::widget([
                                'buttons' => Html::a('<i class="glyphicon glyphicon-trash"></i>&nbsp; Удалить',
                                    ["bulk-delete"],
                                    [
                                        "class" => "btn btn-danger btn-xs",
                                        'role' => 'modal-remote-bulk',
                                        'data-confirm' => false,
                                        'data-method' => false,// for overide yii data api
                                        'data-request-method' => 'post',
                                        'data-confirm-title' => 'Are you sure?',
                                        'data-confirm-message' => 'Do you really want to delete this item?'
                                    ]),
                            ]) .
                            '<div class="clearfix"></div>' : '',
                      ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id"=>"ajaxCrudModal",
    "footer"=>"",// always need it for jquery plugin
])?>
<?php Modal::end(); ?>

<?php

try {
    $this->registerJsFile('/plugins/lightbox/js/lightbox.min.js');
} catch (\yii\base\InvalidConfigException $e) {
    echo $e->getMessage();
}