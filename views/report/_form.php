<?php

use app\models\ApparatusToClient;
use app\models\Client;
use app\models\constants\UsersConstants;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Report */
/* @var $form yii\widgets\ActiveForm */
?>

    <div class="report-form">

        <?php $form = ActiveForm::begin(); ?>


        <?php
        if (UsersConstants::isSuperAdmin() && !$model->isNewRecord) {
            try {
                $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => (new Client)->getList(),
                    'class' => 'form-control disabled'
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            }
            echo $form->field($model, 'status')->dropDownList($model->getStatusesList());
        }
        ?>

        <?php
        try {
            echo $form->field($model, 'apparatus_id')->widget(Select2::class, [
                'data' => (new ApparatusToClient)->getApparatusListForClient($model->client_id),
                'options' => [
                    'id' => 'apparatus-select',
                    'placeholder' => 'Выберите аппарат'
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>

        <div class="row monochrome-counter">
            <div class="col-md-12">
                <?= $form->field($model, 'counter_1')->textInput()->label('Показания счетчика') ?>
            </div>
        </div>
        <div class="row colored-counter" style="display: none;">
            <div class="col-md-6">
                <?= $form->field($model, 'counter_3')->textInput()->label('Счетчик отпечатков (черный)') ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'counter_2')->textInput() ?>
            </div>
        </div>

        <?php
        try {
            echo $form->field($model, 'photo')->widget(FileInput::class, [
                'options' => [
//                    'accept' => 'image/*',
                    'multiple' => false,
                    //            'class' => 'image_input',
                    'id' => 'inputFile',
                ],
                'pluginOptions' => [
                    'showPreview' => false,
                    //            'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'initialCaption' => $model->photo ? pathinfo($model->photo, PATHINFO_BASENAME) : '',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' => 'Выберите фото счетчика/ов',
                    'allowedFileExtensions' => ['jpg', 'jpeg', 'tiff', 'pdf', 'png', 'gif'],
                    //            'maxFileCount' => 1,
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'report_month')->dropDownList($model->getMonthList(), [
                    'value' => date('n', time()),
                ]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'report_year')->dropDownList($model->getYearList(), [
                    'value' => date('Y', time()),
                ]) ?>

            </div>
        </div>
        <?php if ($model->isNewRecord): ?>
            <div class="row">
                <div class="col-md-6 col-md-offset-6">
                    <div class="text-right warning-info">
                        <small>Помощь - <?= Html::a('service@kcepokc.ru', 'mailto:service@kcepokc.ru') ?></small>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>
        <?= $form->field($model, 'apparatus_type')->hiddenInput()->label(false) ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>

<?php
$script = <<<JS
showCounterInput();
$(document).on('change', '#apparatus-select', function() {
   showCounterInput()
});

function showCounterInput() {
   var id = $('#apparatus-select').val();
    var mono_block = $('.monochrome-counter');
    var color_block = $('.colored-counter');
    $.get(
        '/apparatus/get-type',
        {id:id},
        function(res) {
            if (res > 0){
                $('[name="Report[apparatus_type]"]').val(res);
                if (res === 1){
                    color_block.hide();
                    mono_block.fadeIn(400);
                } else {
                    mono_block.hide();
                    color_block.fadeIn(400);
                }
            }
            return true;
        }
    )
}
JS;

$this->registerJs($script);
