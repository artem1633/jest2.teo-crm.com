<?php

use app\models\Report;
use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Report */

CrudAsset::register($this);

?>

<div class="report-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'created_at:datetime',
                'sented_at:datetime',
                [
                    'attribute' => 'apparatus_to_client_id',
                    'value' => $model->apparatusToClient->apparatus->getApparatusFullName() . ' №' . $model->apparatusToClient->serial_number,
                    'label' => 'Аппарат'
                ],
                [
                    'attribute' => 'counter_1',
                    'value' => function (Report $model) {
                        return $model->counter_1;
                    },
                    'label' => $model->apparatus->type == 1 ? 'Счетчик отпечатков' : 'Счетчик отпечатков (черный)'
                ],
                'counter_2',
                [
                    'attribute' => 'photo',
                    'value' => $model->getCountersPhoto(),
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'report_month',
                    'value' => $model->getMonthName($model->report_month),
                    'format' => 'raw'
                ],
                [
                    'attribute' => 'status',
                    'value' => $model->getStatusesList()[$model->status],
                    'format' => 'raw',
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>


<?php
$request = Yii::$app->request;

if (!$request->isAjax && $request->isGet) {
    echo Html::a('Подтвердить', ['/report/change-status', 'id' => $model->id, 'status' => $model::STATUS_APPROVED], [
        'class' => $model->status >= $model::STATUS_APPROVED ? 'hidden' : 'btn btn-info pull-right',
        'title' => 'Подтверждение показаний',
        'data-toggle' => 'tooltip',
        'role' => 'modal-remote',
        'data-confirm-title' => 'Подтвердить показания?',
        'data-confirm-message' => 'Подтвердите показания счетчика/ов',
        'data-confirm-ok' => 'Подтверить',
        'data-confirm-cancel' => 'Отмена',
    ]);
    try {
        $this->registerJsFile('/plugins/lightbox/js/lightbox.min.js');
    } catch (\yii\base\InvalidConfigException $e) {
        echo $e->getMessage();
    }
}

?>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
