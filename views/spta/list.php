<?php

use yii\helpers\Html;

/** @var \app\models\Order $model */

$counter = 1;
?>
<div class="col-sm-12">

</div>


<div class="row">
    <?php /** @var \app\models\Spta $spta */
    foreach ($model->sptas as $spta):?>
        <div class="col-sm-4">
            <div class="well" style="position: relative">
                <div class="complaint-text">
                    <?= $counter . '. Тип: ' . $spta->typeLabel . ' <br>' . $spta->name . ' (' . $spta->num . 'шт.)'; ?>
                </div>
                <?= Html::a('<span class="fa fa-edit edit-complaint"></span>',
                    ['/spta/update', 'id' => $spta->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 2rem; font-size: 1.5rem'
                    ]) ?>
                <?= Html::a('<span class="fa fa-close delete-complaint"></span>',
                    ['/spta/delete', 'id' => $spta->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 0.5rem; font-size: 1.5rem; color: red;',
                        'title' => 'Удалить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите удаление работы',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',
                    ]) ?>
            </div>
        </div>
        <?php $counter++; ?>
    <?php endforeach; ?>
</div>
