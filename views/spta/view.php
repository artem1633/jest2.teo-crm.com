<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Spta */
?>
<div class="spta-view">
 
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'created_at',
            'type',
            'name',
            'num',
        ],
    ]) ?>

</div>
