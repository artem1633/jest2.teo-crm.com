<?php

/* @var $this yii\web\View */
/* @var $model app\models\Spta */
?>
<div class="spta-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
