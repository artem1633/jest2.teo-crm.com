<?php

use yii\helpers\Html;

/** @var \app\models\Order $model */

$counter = 1;
?>
<div class="col-sm-12">

</div>


<div class="row">
    <?php /** @var \app\models\Work $work */
    foreach ($model->works as $work):?>
        <div class="col-sm-12">
            <div class="well" style="position: relative">
                <div class="complaint-text">
                    <?= $counter . '. ' . $work->statusLabel . ' <br>' . $work->content; ?>
                </div>
                <?= Html::a('<span class="fa fa-edit edit-complaint"></span>',
                    ['/work/update', 'id' => $work->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 2rem; font-size: 1.5rem'
                    ]) ?>
                <?= Html::a('<span class="fa fa-close delete-complaint"></span>',
                    ['/work/delete', 'id' => $work->id], [
                        'role' => 'modal-remote',
                        'style' => 'position: absolute; top: 0.5rem; right: 0.5rem; font-size: 1.5rem; color: red;',
                        'title' => 'Удалить',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите удаление работы',
                        'data-confirm-ok' => 'Удалить',
                        'data-confirm-cancel' => 'Отмена',
                    ]) ?>
            </div>
        </div>
        <?php $counter++;?>
    <?php endforeach; ?>
</div>
