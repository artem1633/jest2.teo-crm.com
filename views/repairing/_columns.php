<?php

use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use app\models\Repairing;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'format' => 'datetime',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'repairing-column',
            'style' => 'cursor: pointer;'
        ]
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'id',
//        'label' => '№',
//        'width' => '100px',
//        'vAlign' => 'middle',
//        'hAlign' => 'center',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'client_id',
        'content' => function (Repairing $model) {
            return $model->client->official_name ?? null;
        },
        'visible' => UsersConstants::isSuperAdmin(),
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'repairing-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_id',
        'content' => function (Repairing $model) {
            /** @var \app\models\Apparatus $apparatus */
            $apparatus = $model->apparatus ?? null;
            /** @var \app\models\Client $client */
            $client = $model->client;
            $str = '';
            if ($apparatus) {
                $str = $apparatus->getApparatusFullName() ?? '';
            }

            if ($client) {
                $str .= ' №' . ApparatusToClient::findOne([
                        'client_id' => $model->client_id,
                        'apparatus_id' => $model->apparatus_id
                    ])->serial_number ?? null;
            }

            return $str;
        },
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'repairing-column',
            'style' => 'cursor: pointer;'
        ]
    ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'description',
//        'hAlign' => 'center',
//        'vAlign' => 'middle',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'counter_1',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'repairing-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'done_at',
        'hAlign' => 'center',
        'vAlign' => 'middle',
        'contentOptions' => [
            'class' => 'repairing-column',
            'style' => 'cursor: pointer;'
        ]
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
//    [
//        'class' => '\kartik\grid\DataColumn',
//        'attribute' => 'status',
//        'filter' => (new Repairing())->getStatusList(),
//        'content' => function (Repairing $model) {
//            $status_name = $model->getStatusName($model->status);
//            if ($model->status == $model::STATUS_DRAFT && $model->client_id == Yii::$app->user->identity->client_id) {
//                $btn = Html::a('Отправить заявку', ['to-new', 'id' => $model->id], [
//                        'class' => 'btn btn-small btn-success',
//                        'title' => 'Отправить заявку',
//                        'data-toggle' => 'tooltip',
//                        'role' => 'modal-remote',
//                        'data-confirm-title' => 'Отправить заявку диспетчеру?',
//                        'data-confirm-message' => 'Подтвердите заявку',
//                        'data-confirm-ok' => 'Отправить заявку',
//                        'data-confirm-cancel' => 'Отмена',
//                    ]);
//            } elseif (UsersConstants::isSuperAdmin() && $model->status > $model::STATUS_DRAFT) {
//                //Для админа показываем кнопки в работу или выполнено
//                $btn = $model->getButton();
//            } else {
//                $btn = '';
//            }
//
//            return '<p class="margin-bottom: 0.5rem;"><b>' . $status_name . '</b></p>' . $btn;
//        },
//        'format' => 'raw',
//        'hAlign' => 'center',
//        'vAlign' => 'middle',
//    ],
//    [
//        'class' => 'kartik\grid\ActionColumn',
//        'dropdown' => false,
//        'vAlign' => 'middle',
////        'template' => '{edit} {delete}',
//        'urlCreator' => function ($action, $model, $key) {
//            return Url::to([$action, 'id' => $key]);
//        },
//        'buttons' => [
//            'update' => function ($url, Repairing $model) {
//                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isSuperAdmin()) {
//                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
//                        'role' => 'modal-remote',
//                        'title' => 'Редактировать',
//                        'data-toggle' => 'tooltip',
//                    ]);
//                } else {
//                    return null;
//                }
//            },
//            'delete' => function ($url, Repairing $model) {
//                if ($model->status === $model::STATUS_DRAFT || UsersConstants::isSuperAdmin()) {
//                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
//                        'role' => 'modal-remote',
//                        'title' => 'Удалить заявку',
//                        'data-confirm' => false,
//                        'data-method' => false,// for overide yii data api
//                        'data-request-method' => 'post',
////                        'data-toggle' => 'tooltip',
//                        'data-confirm-title' => 'Вы уверены?',
//                        'data-confirm-message' => 'Подтвердите удаление заказа',
//                        'data-confirm-ok' => 'Удалить',
//                        'data-confirm-cancel' => 'Отмена',
//                    ]);
//                } else {
//                    return null;
//                }
//            }
//        ],
//        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip', 'class' => 'view-repairing'],
//    ],

];   