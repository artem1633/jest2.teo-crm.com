<?php

/* @var $this yii\web\View */
/* @var $model app\models\Repairing */
?>
<div class="repairing-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
