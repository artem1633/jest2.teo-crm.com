<?php

use app\models\Apparatus;
use app\models\ApparatusToClient;
use app\models\Client;
use app\models\constants\UsersConstants;
use kartik\file\FileInput;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
/* @var $order_to_cons app\models\OrderToConsumables */
/* @var $order_to_cons_models app\models\OrderToConsumables[] */

$counter = 1;
if (\app\models\constants\UsersConstants::isSuperAdmin()){
    $client_id = $model->client_id;
} else {
    $client_id = null;
}
?>

    <div class="order-form">

        <?php $form = ActiveForm::begin(); ?>
        <?php
        if (UsersConstants::isSuperAdmin()){
            try {
                echo $form->field($model, 'client_id')->widget(Select2::class, [
                    'data' => (new Client)->getList(),
                    'options' => [
                        'placeholder' => 'Выберите клиента',
                        'disabled' => !$model->isNewRecord,
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            }

            echo $form->field($model, 'status')->dropDownList($model->getStatusList(), ['prompt' => 'Выберите статус']);
        }
        ?>
        <?php
        try {
            echo $form->field($model, 'apparatus_id')->widget(Select2::class, [
                'data' => (new ApparatusToClient)->getApparatusListForClient($client_id),
                'options' => [
                    'placeholder' => 'Выберите аппарат'
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>

        <div class="row">
            <?php if(!$model->isNewRecord && $model->apparatus->type == Apparatus::TYPE_MONOCHROME): ?>
            <div class="col-xs-12">
                <?= $form->field($model, 'counter_1')->textInput()?>
            </div>

            <?php else: ?>
                <div class="col-xs-4">
                    <?= $form->field($model, 'counter_1')->textInput()?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'counter_2')->textInput()?>
                </div>
                <div class="col-xs-4">
                    <?= $form->field($model, 'counter_3')->textInput()?>
                </div>
            <?php endif;?>
        </div>

        <?= $form->field($model, 'description')->textarea(['rows' => 6])?>

        <?php
        try {
            echo $form->field($model, 'images',
                ['template' => '{input}'])->widget(FileInput::class, [
                'options' => [
                    'accept' => 'image/*',
                    'multiple' => true,
                    'class' => 'image_input',
                    'id' => 'inputFile',
                ],
                'pluginOptions' => [
                    'showPreview' => false,
//                    'showCaption' => false,
                    'showRemove' => false,
                    'showUpload' => false,
                    'browseClass' => 'btn btn-primary btn-block',
                    'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
                    'browseLabel' => 'Выберите фото/сканы проблемных отпечатков',
                    'allowedFileExtensions' => ['jpg', 'jpeg', 'tiff', 'pdf', 'png'],
                    'maxFileCount' => 5,
                ]
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>

        <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>

        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
