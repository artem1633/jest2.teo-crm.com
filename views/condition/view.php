<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Condition */
?>
<div class="condition-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'order_id',
                'created_at',
                'description:ntext',
            ],
        ]);
    } catch (Exception $e) {
        echo $e->getMessage();
    } ?>

</div>
