<?php

/* @var $this yii\web\View */
/* @var $model app\models\Condition */

?>
<div class="condition-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
