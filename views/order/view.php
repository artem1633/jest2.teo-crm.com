<?php

use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

?>
<div class="order-view">
    <div class="row">
        <div class="col-sm-12">
            <?= \yii\helpers\Html::a('<span class="fa fa-arrow-left"></span>&nbsp;Назад', Yii::$app->request->referrer,
                [
                    'class' => 'btn btn-info',
                    'style' => 'margin-bottom: 2rem;'
                ]) ?>
        </div>
    </div>
    <!--Карточка заявки-->
    <div class="row">
        <div class="col-sm-12">
            <?php
            try {
                echo DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id',
                        'created_at:datetime',
                        [
                            'attribute' => 'client_id',
                            'value' => $model->client ? $model->client->official_name : '',
                        ],
                        'counter_1',
                        'counter_2',
                        'counter_3',
                        'done_at:date',
                        [
                            'attribute' => 'status',
                            'value' => $model->statusName,
                        ],
                        [
                            'attribute' => 'apparatus_client_id',
                            'value' => $model->apparatus ? $model->apparatus->getApparatusFullName() : '',
                        ],
                        [
                            'attribute' => 'a_condition',
                            'label' => 'Состояние аппарата',
                            'value' => $model->condition ? $model->condition->description : '',
                        ],
                        [
                            'attribute' => 'service_type',
                            'value' => $model->serviceTypeLabel,
                        ],
                        [
                            'attribute' => 'work_type',
                            'value' => $model->workTypeLabel,
                        ],
                        'total_time',
                        'account_number',
                        [
                            'attribute' => 'manager_id',
                            'value' => $model->manager ? $model->manager->fio : '',
                        ],
                        [
                            'attribute' => 'engineer_id',
                            'value' => $model->engineer ? $model->engineer->fio : '',
                        ],
                        [
                            'attribute' => 'is_repeat',
                            'value' => $model->is_repeat ? 'Да' : 'Нет',
                        ],

                    ],
                ]);
            } catch (Exception $e) {
                echo $e->getMessage();
            } ?>
        </div>
    </div>
    <!--Жалобы-->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-inverse order-index-search">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                            <i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Жалобы</h4>
                </div>
                <div class="panel-body" style="display:block">
                    <?php
                    $counter = 1;
                    /** @var \app\models\Complaint $complaint */
                    foreach($model->complaints as $complaint):?>
                        <div style="border-bottom: 1px solid lightgrey; padding: 5px;"><?= $counter . '. ' . $complaint->text; ?></div>
                    <?php
                    $counter++;
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!--Работы-->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-inverse order-index-search">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                            <i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">Работы</h4>
                </div>
                <div class="panel-body" style="display:block">
                    <?php
                    $counter = 1;
                    /** @var \app\models\Work $work */
                    foreach($model->works as $work):?>
                        <div style="border-bottom: 1px solid lightgrey; padding: 5px;"><?= $counter . '. ' . $work->content . ' (' . $work->statusLabel . ')'; ?></div>
                        <?php
                        $counter++;
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <!--ЗиП-->
    <div class="row">
        <div class="col-sm-12">
            <div class="panel panel-inverse order-index-search">
                <div class="panel-heading">
                    <div class="panel-heading-btn">
                        <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                            <i class="fa fa-minus"></i></a>
                    </div>
                    <h4 class="panel-title">ЗиП</h4>
                </div>
                <div class="panel-body" style="display:block">
                    <?php
                    $counter = 1;
                    /** @var \app\models\Spta $spta */
                    foreach($model->sptas as $spta):?>
                        <div style="border-bottom: 1px solid lightgrey; padding: 5px;"><?= $counter . '. ' . $spta->name . ' (' . $spta->num . ' шт.)'; ?></div>
                        <?php
                        $counter++;
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <?= \yii\helpers\Html::a('<span class="fa fa-arrow-left"></span>&nbsp;Назад', Url::previous(), [
                'class' => 'btn btn-info'
            ]) ?>
        </div>
    </div>

</div>
