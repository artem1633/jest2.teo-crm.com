<?php

use johnitvn\ajaxcrud\CrudAsset;
use kartik\switchinput\SwitchInput;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\search\OrderSearch */
/* @var $form yii\widgets\ActiveForm */
CrudAsset::register($this);

?>

<div class="meter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/order/index'],
        'method' => 'get',
//        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
//                'offset' => 'col-sm-offset-1',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <?= $form->field($model, 'id')->hiddenInput()->label(false); ?>

        <div class="col-sm-6">
            <?= $form->field($model, 'search_start_date')->input('date')->label('c') ?>
        </div>
        <div class="col-sm-6">
            <?= $form->field($model, 'search_end_date')->input('date')->label('по') ?>
        </div>

    </div>
    <div class="row">
        <div class="col-sm-6">
            <?php
            try {
                echo $form->field($model, 'search_type_date')->widget(SwitchInput::class, [
                    'options' => [
                    ],
                    'pluginOptions' => [
                        'onText' => 'Поиск по дате выполнения',
                        'offText' => 'Поиск по дате создания'
                    ]
                ])->label(false);
            } catch (Exception $e) {
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-sm-6">
            <?= Html::submitButton('Показать', [
                'class' => 'btn btn-primary btn-block',
            ]) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
