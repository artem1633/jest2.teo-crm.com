<?php

use app\models\constants\UsersConstants;
use app\models\Order;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
//    [
//        'class' => 'kartik\grid\SerialColumn',
//        'width' => '30px',
//    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'id',
        'width' => '30px',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'header' => 'Дата <br> создания',
        'format' => 'datetime',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'done_at',
        'header' => 'Дата <br> закрытия',
        'format' => 'date',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'header' => 'Клиент',
        'attribute' => 'client_name',
        'content' => function (Order $model) {
            return $model->client ? $model->client->official_name : '';
        },
        'label' => 'Клиент',
        'vAlign' => 'middle',
        'contentOptions' => [
            'style' => 'min-width: 200px'
        ]
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'counter_1',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'counter_2',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'counter_3',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'photos',
//    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'status',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_name',
        'header' => 'Аппарат',
        'content' => function (Order $model) {
            return $model->apparatusToClient ? $model->apparatusToClient->apparatus->getApparatusFullName() : '';
        },
        'vAlign' => 'middle',
        'contentOptions' => [
            'style' => 'min-width: 200px'
        ],
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'serial_number',
        'header' => 'Серийный<br>номер',
        'content' => function (Order $model) {
            return $model->apparatusToClient ? $model->apparatusToClient->serial_number : '';
        },
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'service_type',
        'header' => 'Тип услуг',
        'filter' => (new Order())->getServiceTypeList(),
        'content' => function (Order $model) {
            return $model->serviceTypeLabel;
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'work_type',
        'header' => 'Тип работ',
        'filter' => (new Order())->getTypeWorkList(),
        'content' => function (Order $model) {
            return $model->workTypeLabel;
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'account_number',
        'header' => 'Номер <br> счета',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'parent_order_id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'total_time',
        'header' => 'Затраченное <br> время (час.)',
        'vAlign' => 'middle',
        'hAlign' => 'center',
    ],

    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'manager_id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'engineer_fio',
        'header' => 'Инженер',
        'content' => function (Order $model) {
            return $model->engineer ? $model->engineer->fio : null;
        },
        'vAlign' => 'middle',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'is_repeat',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{get-sl}&nbsp;&nbsp;{copy-order}&nbsp;&nbsp;{view}&nbsp;&nbsp;{finish}&nbsp;&nbsp;{update}&nbsp;&nbsp;{delete}',
        'vAlign' => 'middle',
//        'width' => '150px',
        'urlCreator' => function ($action, $model, $key, $index) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'get-sl' => function ($action, Order $model) {
                if ($model->status != $model::STATUS_DONE && $model->status != $model::STATUS_CANCEL) {
                    return Html::a('<span class="glyphicon glyphicon-print"></span>', [$action], [
                        'title' => 'Распечатать сервисный лист',
                        'target' => '_blank',
                        'data-toggle' => 'tooltip',
                        'data-pjax' => 0,
                    ]);
                }
                return null;
            },
            'copy-order' => function ($action, Order $model) {
                return Html::a('<span class="glyphicon glyphicon-copy"></span>', [$action], [
                    'title' => 'Создать заявку на основании текущей заявки',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'data-pjax' => 0,
                ]);
            },
            'finish' => function ($action, Order $model) {
                if ($model->status == $model::STATUS_WORK) {
                    return Html::a('<span class="glyphicon glyphicon-saved"></span>', [$action], [
                        'title' => 'Завершить заявку',
                        'style' => 'color: green',
                        'data-toggle' => 'tooltip',
                        'role' => 'modal-remote',
                        'data-pjax' => 1,
                    ]);
                }
                return null;
            },
            'update' => function ($action, Order $model) {
                if ($model->status == $model::STATUS_NEW || UsersConstants::isSuperAdmin()) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', [$action], [
                        'title' => 'Редактировать',
                        'data-toggle' => 'tooltip',
                        'data-pjax' => 0,
                    ]);
                }
                return null;
            },

            'delete' => function ($action, Order $model) {
                if ($model->status == $model::STATUS_NEW || $model->status == $model::STATUS_WORK) {
                    return Html::a('<span class="glyphicon glyphicon-trash"></span>', [$action], [
                        'role' => 'modal-remote',
                        'title' => 'Отменить заявку',
                        'style' => 'color: red',
                        'data-confirm' => false,
                        'data-method' => false,// for overide yii data api
                        'data-request-method' => 'post',
                        'data-toggle' => 'tooltip',
                        'data-confirm-title' => 'Вы уверены?',
                        'data-confirm-message' => 'Подтвердите отмену заявки',
                        'data-confirm-ok' => 'Отменить заявку',
                        'data-confirm-cancel' => 'Закрыть окно',
                    ]);
                }
                return null;
            },

        ],
        'viewOptions' => ['title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['title' => 'Редактирование', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'style' => 'color: red',
            'title' => 'Отменить заявку',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Вы уверены?',
            'data-confirm-message' => 'Подтвердите отмену заявки',
            'data-confirm-ok' => 'Отменить заявку',
            'data-confirm-cancel' => 'Закрыть окно',
        ],
        'contentOptions' => [
            'style' => 'min-width: 150px;'
        ]
    ],

];   