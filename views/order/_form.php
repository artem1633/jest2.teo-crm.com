<?php

use app\models\ApparatusToClient;
use app\models\Client;
use app\models\Users;
use johnitvn\ajaxcrud\CrudAsset;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $condition_model \app\models\Condition */
/* @var $form yii\widgets\ActiveForm */
/* @var $complaintSearchModel \app\models\search\ComplaintSearch */
/* @var $complaintDataProvider \yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>

<?php Pjax::begin(['id' => 'order-pjax-container', 'enablePushState' => false]) ?>

<?php
    echo "<h4 class='text-right'>Создание заявки</h4>";
?>
    <div class="container order-container">
        <div class="order-form">

            <?php $form = ActiveForm::begin(); ?>
            <div class="row">
                <div class="btn-group">
                    <?= \yii\helpers\Html::a('<span class="fa fa-arrow-left"></span>&nbsp;Назад',
                        Yii::$app->request->referrer, [
                            'class' => 'btn btn-info',
                            'style' => 'margin-bottom: 2rem;'
                        ]) ?>
                    <?= Html::submitButton('Сохранить заявку',
                        ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                </div>
            </div>
            <div class="row">
                <?php if ($model->status == $model::STATUS_WORK || $model->status == $model::STATUS_DONE): ?>

                    <div class="col-sm-12">
                        <?= $form->field($condition_model,
                            'description')->textarea(['rows' => 3])->label('Текущее состояние аппарата') ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'total_time')->textInput() ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'done_at')->input('date', [
                            'value' => $model->done_at ? explode(' ', $model->done_at)[0] : '',
                        ]) ?>
                    </div>
                    <div class="col-sm-4">
                        <?= $form->field($model, 'account_number')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Выполненные работы
                                <?= Html::a('<span class="fa fa-plus"></span> Добавить',
                                    ['/work/create', 'id' => $model->id], [
                                        'role' => 'modal-remote',
                                        'class' => 'btn btn-default btn-xs pull-right',
                                        'style' => 'margin-bottom: 1rem;'
                                    ]) ?>
                            </div>
                            <div class="panel-body">
                                <?php Pjax::begin(['id' => 'work-pjax-container', 'enablePushState' => false]) ?>
                                <?= $this->render('@app/views/work/list', [
                                    'model' => $model,
                                ]) ?>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="panel panel-success">
                            <div class="panel-heading">
                                Установленные ЗиП
                                <?= Html::a('<span class="fa fa-plus"></span> Добавить',
                                    ['/spta/create', 'id' => $model->id], [
                                        'role' => 'modal-remote',
                                        'class' => 'btn btn-default btn-xs pull-right',
                                        'style' => 'margin-bottom: 1rem;'
                                    ]) ?>
                            </div>
                            <div class="panel-body">
                                <?php Pjax::begin(['id' => 'spta-pjax-container', 'enablePushState' => false]) ?>
                                <?= $this->render('@app/views/spta/list', [
                                    'model' => $model,
                                ]) ?>
                                <?php Pjax::end() ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
            <!--Клиент-->
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-sm-11">
                    <?php
                    try {
                        echo $form->field($model, 'client_id')->widget(Select2::class, [
                            'data' => (new Client)->getList(),
                            'options' => [
                                'id' => 'order-client',
                                'prompt' => 'Выберите заказчика'
                            ]
                        ])->label('Наименование заказчика');
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    } ?>
                </div>
                <div class="col-sm-1">
                    <?= Html::a('<span class="fa fa-plus"></span>', ['/client/create', 'caller' => '/order/create'], [
                        'class' => 'btn btn-default btn-block',
                        'role' => 'modal-remote'
                    ]) ?>
                </div>
            </div>
            <!--Аппарат-->
            <div class="row" style="display: flex; align-items: center;">
                <div class="col-sm-10">
                    <?php Pjax::begin(['id' => 'apparatus-client-container']) ?>
                    <?php
                    try {
                        echo $form->field($model, 'apparatus_client_id')->widget(Select2::class, [
                            'data' => (new ApparatusToClient())->getApparatusListForClient($model->client_id),
                            'options' => [
                                'id' => 'apparatus-client',
                                'prompt' => 'Выберите аппарат',
                            ]
                        ]);
                    } catch (Exception $e) {
                        echo $e->getMessage();
                    } ?>
                    <?php Pjax::end() ?>
                </div>
                <div class="col-sm-2 btn-group btn-group-justified" role="group" style="max-width: 200px;">
                    <?= Html::a('<span class="fa fa-plus"></span>',
                        [
                            '/apparatus-to-client/create',
                            'caller' => '/order/update?id=' . $model->id,
                            'id' => $model->client_id,
                            'pjaxReloadContainer' => '#apparatus-client-container',
                        ], [
                            'class' => 'btn btn-default',
                            'role' => 'modal-remote',
                            'id' => 'add-apparatus-btn',
                            'style' => $model->client_id ? '' : 'display:none',
                            'title' => 'Добавить аппарат клиенту'
                        ]) ?>
                    <?= Html::a('<span class="fa fa-plus-square"></span>',
                        ['/apparatus/create', 'caller' => '/order/update?id=' . $model->id], [
                            'class' => 'btn btn-default',
                            'role' => 'modal-remote',
                            'id' => 'add-apparatus-model-btn',
//                            'style' => 'display:none;',
                            'title' => 'Добавить модель аппарата'
                        ]) ?>
                </div>
            </div>
            <!--Счетчики-->
            <div class="row">
                <div class="col-sm-4">
                    <?= $form->field($model, 'counter_1')->textInput() ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'counter_2')->textInput() ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'counter_3')->textInput() ?>
                </div>
            </div>
            <!--Инженер-->
            <div class="row">
                <div class="col-sm-12">
                    <?= $form->field($model, 'engineer_id')->dropDownList((new Users)->getEngineerList(),
                        ['prompt' => 'Выберите сотрудника']) ?>
                </div>
            </div>
            <!--Тип услуг и работ-->
            <div class="row" style="display: flex; align-items: center;">

                <div class="col-sm-4">
                    <?= $form->field($model, 'service_type')->dropDownList($model->getServiceTypeList()) ?>
                </div>
                <div class="col-sm-4">
                    <?= $form->field($model, 'work_type')->dropDownList($model->getTypeWorkList()) ?>
                </div>
                <?php if (count($model->complaints) < 3): ?>
                    <div class="col-sm-4">
                        <?= Html::a('Добавить жалобу', ['/complaint/create', 'id' => $model->id], [
                            'class' => 'btn btn-default btn-block',
                            'role' => 'modal-remote',
                        ]) ?>
                    </div>
                <?php endif; ?>
            </div>
            <!--Жалобы-->
            <div class="row">
                <div class="col-sm-12">
                    <div id="complaint-list">
                        <?php Pjax::begin(['id' => 'complaint-pjax-container', 'enablePushState' => false]) ?>
                        <?= $this->render('@app/views/complaint/list', ['model' => $model,]) ?>
                        <?php Pjax::end() ?>
                    </div>
                </div>
            </div>

            <?php if (\app\models\constants\UsersConstants::isSuperAdmin()): ?>
                <?= $form->field($model, 'status')->dropDownList($model->getStatusList()) ?>
            <?php else: ?>
                <?= $form->field($model, 'status')->hiddenInput()->label(false) ?>
            <?php endif; ?>

            <?= $form->field($model, 'parent_order_id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'manager_id')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'is_repeat')->hiddenInput()->label(false) ?>

            <?= $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

            <div class="btn-group">
                <?= \yii\helpers\Html::a('<span class="fa fa-arrow-left"></span>&nbsp;Назад',
                    Yii::$app->request->referrer, [
                        'class' => 'btn btn-info',
                        'style' => 'margin-bottom: 2rem;'
                    ]) ?>
                <?= Html::submitButton('Сохранить заявку',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>

<?php Pjax::end(); ?>

<?php
$script = <<<JS
$(document).ready(function() {
    $(document).on('change','#order-client', function() {
        var client_id = $(this).val();
        var add_apparatus_btn = $('#add-apparatus-btn');
        var apparatus_dropdown = $('#apparatus-client');
        
        add_apparatus_btn.attr('href', '/apparatus-to-client/create?id=' + client_id + '&caller=/order/update?id={$model->id}&pjaxReloadContainer=apparatus-client-container');
        add_apparatus_btn.show();
        
        $('#add-apparatus-model-btn').show();
         $.get(
            '/apparatus-to-client/get-apparatus-for-client',
            {id:client_id},
            function(res) {
                if (res.success === 1){
                    apparatus_dropdown.html(res.data)
                }
            }
        );
         
        $.post(
            '/order/set-client',
            {
                order_id:{$model->id},
                client_id:client_id
            }
        );
        
       
    });
});
JS;

$this->registerJs($script);