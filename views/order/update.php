<?php

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $condition_model app\models\Condition */
/* @var $complaintSearchModel \app\models\search\ComplaintSearch */
/* @var $complaintDataProvider \yii\data\ActiveDataProvider */
?>
<div class="order-update">

    <?= $this->render('_form', [
        'model' => $model,
        'condition_model' => $condition_model,
        'complaintSearchModel' => $complaintSearchModel,
        'complaintDataProvider' => $complaintDataProvider,
    ]) ?>

</div>
