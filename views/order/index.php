<?php

use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var string $append Добавление к названию таблицы */

$this->title = 'Заявки';
$this->params['breadcrumbs'][] = $this->title;


if (strpos($append, 'Текущ')) {
    $path_column = __DIR__ . '/_current_columns.php';
} elseif (strpos($append, 'Закры')) {
    $path_column = __DIR__ . '/_close_columns.php';
} else {
    $path_column = __DIR__ . '/_columns.php';
}

Yii::info($append, 'test');
Yii::info($path_column, 'test');
CrudAsset::register($this);

?>
<div class="panel panel-inverse order-index-search">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse">
                <i class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Фильтр&nbsp;заявок&nbsp;<?= $append; ?></h4>
    </div>
    <div class="panel-body" style="<?= !$searchModel->search_start_date ? 'display: none' : '' ?>">
        <?= $this->render('_search', ['model' => $searchModel]) ?>
    </div>
</div>
<div class="panel panel-inverse order-index">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i
                        class="fa fa-expand"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i
                        class="fa fa-repeat"></i></a>
            <a href="javascript:" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i
                        class="fa fa-minus"></i></a>
        </div>
        <h4 class="panel-title">Заявки&nbsp;<?= $append; ?></h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered table-hover'],
                    'resizableColumns' => true,
//                    'floatHeader'=>true,
//                    'floatHeaderOptions'=>['top'=>'50'],
                    'pjax' => true,
                    'columns' => require($path_column),
                    'toolbar' => [
                        [
                            'content' =>
//                                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create'],
//                                    [
////                                        'role' => 'modal-remote',
//                                        'data-pjax' => 0,
//                                        'title' => 'Добавить аппарат',
//                                        'class' => 'btn btn-default'
//                                    ]) .
                                '{toggleData}' .
                                '{export}'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
