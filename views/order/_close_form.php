<?php

use johnitvn\ajaxcrud\CrudAsset;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $condition_model \app\models\Condition */
/* @var $form yii\widgets\ActiveForm */
/* @var $complaintSearchModel \app\models\search\ComplaintSearch */
/* @var $complaintDataProvider \yii\data\ActiveDataProvider */

CrudAsset::register($this);

?>
    <div class="order-form">

        <?php $form = ActiveForm::begin(); ?>

        <?= $form->field($model, 'status')->hiddenInput(['value' => $model::STATUS_DONE])->label(false) ?>
        <div class="row">
            <div class="col-md-12">
                <?= $form->field($model, 'done_at')->input('date', [
                        'value' => date('Y-m-d', time())
                ])->label(false) ?>
            </div>
        </div>


        <?php if (!Yii::$app->request->isAjax) { ?>
            <div class="form-group">
                <?= Html::submitButton('Сохранить',
                    ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>
        <?php } ?>

        <?php ActiveForm::end(); ?>

    </div>
<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
<?php
$script = <<<JS
$(document).ready(function() {
    $(document).on('change','#order-client', function() {
        var client_id = $(this).val();
        var add_apparatus_btn = $('#add-apparatus-btn');
        var apparatus_dropdown = $('#apparatus-client');
        
        add_apparatus_btn.attr('href', '/apparatus-to-client/create?caller=/order/create&id=' + client_id);
        add_apparatus_btn.show();
        
        $.get(
            '/apparatus-to-client/get-apparatus-for-client',
            {id:client_id},
            function(res) {
                if (res.success === 1){
                    apparatus_dropdown.removeAttr('disabled');
                    apparatus_dropdown.html(res.data)
                }
                return true;
            }
        )
    });
});
JS;

$this->registerJs($script);