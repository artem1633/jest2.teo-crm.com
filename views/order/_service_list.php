<?php

/**@var \app\models\Order $order */
/**@var \app\models\ApparatusToClient $atc */
/**@var \app\models\Client $client */
/**@var \app\models\Recommendation $prev_recommendation */
/**@var \app\models\Work $prev_work */
///**@var \app\models\Spta[] $all_spta */
///**@var \app\models\Work[] $all_works */
/**@var \app\models\Order $prev_order */
/**@var array $spta_and_works */
?>


<div class="sl-front container">
    <div class="sl-header">
        <p>Сервис-центр "ДЖЕСТ", (495) 5802990, service@kcepokc.ru, сервис-деск: http://194.58.121.199</p>
    </div>
    <table style="border-collapse: collapse;">
        <tr>
            <td class="back-gray" style="width: 25%; font-size: 15px; font-weight: bold">Сервисный Лист</td>
            <td style="width: 25%; font-size: 15px; font-weight: bold" align="center">№<?= $order->id ?></td>
            <td class="back-gray" style="width: 25%">Дата формирования</td>
            <td style="width: 25%"><?= Yii::$app->formatter->asDate($order->created_at) ?></td>
        </tr>
        <tr>
            <td class="back-gray">ФИО сервисного инженера</td>
            <td colspan="3" style="font-weight: bold"><?= $order->engineer ? $order->engineer->fio : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Наименование Заказчика</td>
            <td colspan="3"><?= $client ? $client->official_name : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Контактное лицо</td>
            <td><?= $client ? $client->contact_person : '' ?></td>
            <td class="back-gray">Телефон</td>
            <td><?= $client ? $client->phone_number : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Адрес</td>
            <td colspan="3"><?= $client ? $client->address : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray" rowspan="4">Модель</td>
            <td rowspan="4" style="font-weight: bold"><?= $atc ? $atc->apparatus->getApparatusFullName() : ''; ?></td>
            <td class="back-gray">Серийный номер</td>
            <td><?= $atc ? $atc->serial_number : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Счетчик общий</td>
            <td><?= $order ? $order->counter_1 : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Счетчик ч/б</td>
            <td><?= $order ? $order->counter_2 : '' ?></td>
        </tr>
        <tr>
            <td class="back-gray">Счетчик цвет</td>
            <td><?= $order ? $order->counter_3 : '' ?></td>
        </tr>
        <tr>
            <td align="center" colspan="2"><?= $order ? $order->serviceTypeLabel : '' ?></td>
            <td align="center" colspan="2"><?= $order ? $order->workTypeLabel : '' ?></td>
        </tr>
    </table>
    <table>
        <tr>
            <td class="back-gray" style="width: 25%" rowspan="<?= count($order->complaints) ?>">Жалобы заказчика</td>
            <?php
            $counter = 1;
            /** @var \app\models\Complaint $complaint */
            foreach ($order->complaints as $complaint): ?>
            <?php if ($counter == 1): ?>
            <td style="width: 3%"><?= $counter; ?></td>
            <td><?= $complaint->text ?></td>
        </tr>
        <?php else: ?>
            <tr>
                <td><?= $counter; ?></td>
                <td><?= $complaint->text ?></td>
            </tr>
        <?php endif; ?>
        <?php $counter++; ?>
        <?php endforeach; ?>
        <tr>
            <td class="back-gray" rowspan="3">Текущее состояние оборудования и обнаруженные неисправности</td>
            <td style="width: 3%; padding: 7px">1</td>
            <td></td>
        </tr>
        <?php for ($i = 2; $i < 4; $i++): ?>
            <tr>
                <td style="padding: 7px"><?= $i; ?></td>
                <td></td>
            </tr>
        <?php endfor; ?>

        <tr>
            <td class="back-gray">Предыдущие рекомендации<br><?= ($prev_order)
                    ? 'СЛ от '
                    . Yii::$app->formatter->asDate($prev_order->created_at)
                    . ' №' . $prev_order->id : '' ?></td>
            <td colspan="2"><?= $prev_recommendation; ?></td>
        </tr>
    </table>
    <table>
        <thead>
        <tr class="back-gray">
            <th style="width: 3%">№</th>
            <th>Работы/Рекомендации</th>
            <th style="width: 10%">Выполненные</th>
            <th style="width: 10%">Необходимые</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 1; $i < 7; $i++): ?>
            <tr>
                <td style="padding: 7px;">&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
    <table>
        <thead>
        <tr class="back-gray">
            <th style="width: 3%;">№</th>
            <th>ЗИП</th>
            <th style="width: 10%">Установленные</th>
            <th style="width: 10%">Необходимые</th>
        </tr>
        </thead>
        <tbody>
        <?php for ($i = 1; $i < 7; $i++): ?>
            <tr>
                <td style="padding: 7px;">&nbsp;</td>
                <td></td>
                <td></td>
                <td></td>
            </tr>
        <?php endfor; ?>
        </tbody>
    </table>
    <table>
        <tr>
            <td class="back-gray" style="width: 40%" rowspan="2">Общее кол-во затраченных часов на данную заявку</td>
            <td style="width: 10%" rowspan="2"></td>
            <td class="back-gray" style="width: 40%">Аппарат полностью готов</td>
            <td></td>
        </tr>
        <tr>
            <td class="back-gray" style="font-size: 10px;">Апппарат не готов, требует продолжения ремонта</td>
            <td></td>
        </tr>
        <tr>
            <td class="back-gray">Дата закрытия заявки</td>
            <td></td>
            <td colspan="2">Счет №</td>
        </tr>
        <tr>
            <td style="padding: 7px; font-weight: bold;" colspan="2">Инженер
                ________________________________/______________/
            </td>
            <td style="padding: 7px; font-weight: bold;" colspan="2">Принял
                ________________________________/_______________/
            </td>
        </tr>
    </table>
</div>
<pagebreak/>
<div class="sl-back container">
    <div class="sl-header">
        <p>Перечень проделанных работ и установленных
            ЗиП <?= $atc ? ' для ' . $atc->apparatus->getApparatusFullName() : ''; ?></p>
    </div>
    <table>
        <thead>
        <tr>
            <th> Номер заявки</th>
            <th> Дата<br>закрытия</th>
            <th> Работа/Деталь</th>
            <th> Кол.</th>
            <th> Счетчик<br>общий</th>
            <th> Счетчик<br>черный</th>
            <th> Счетчик<br>цветной</th>
            <th> Инженер</th>
        </tr>
        </thead>
        <tbody>
        <?php if (isset($spta_and_works['order'])): ?>
            <?php foreach ($spta_and_works['order'] as $order_id => $data): ?>
                <?php
                $span = count($data['data']);
                if (!$span) {
                    $span = 1;
                }

                Yii::info($span, 'test');
                ?>
                <tr>
                    <td rowspan="<?= $span; ?>" style="width: 15px"><?= $order_id ?></td>
                    <td rowspan="<?= $span; ?>"
                        style="width: 15px"><?= Yii::$app->formatter->asDate($data['done_at']) ?></td>
                    <td style="width: 50%"><?= count($data['data']) > 0 ? $data['data'][0]['description'] : '' ?></td>
                    <td style="width: 15px"><?= count($data['data']) > 0 ? $data['data'][0]['num'] : '' ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c1'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c2'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c3'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['engineer'] ?></td>
                </tr>
                <?php
                if (count($data)) {
                    array_shift($data['data']);
                    Yii::info($data['data'], 'test');
                }
                ?>
                <?php foreach ($data['data'] as $info): ?>
                    <tr>
                        <td style="width: 50%"><?= $info['description'] ?></td>
                        <td style="width: 15px"><?= $info['num'] ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
</div>
