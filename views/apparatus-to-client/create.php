<?php

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */

?>
<div class="apparatus-to-client-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
