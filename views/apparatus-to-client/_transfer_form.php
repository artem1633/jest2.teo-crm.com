<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apparatus-to-client-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-md-6">
            <?php
            try {
                echo $form->field($model, 'owner')->textInput([
                        'disabled' => true
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-md-6">
            <?php
            try {
                echo $form->field($model, 'new_owner_id')->widget(Select2::class, [
                    'data' => (new \app\models\Client())->getList(),
                    'options' => [
                        'placeholder' => 'Выберите получателя аппарата'
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
    <?= $form->field($model, 'client_id')->hiddenInput()->label(false) ?>

    <?php ActiveForm::end(); ?>

</div>
