<?php

use app\models\ApparatusToClient;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */
?>
<div class="apparatus-to-client-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                [
                    'attribute' => 'apparatus_id',
                    'value' =>  $model->apparatus ? $model->apparatus->getApparatusFullName() : 'No name',
                ],
                [
                    'attribute' => 'client_id',
                    'value' => $model->client ? $model->client->official_name : 'No client',
                ],
                'notes:ntext',
                [
                    'attribute' => 'is_owner',
                    'value' => function (ApparatusToClient $model) {
                        if ($model->is_owner) {
                            return 'Да';
                        }
                        return 'Нет';
                    }
                ],
                'serial_number',
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
