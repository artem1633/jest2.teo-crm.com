<?php

/**@var \app\models\Order $order */
/**@var \app\models\ApparatusToClient $atc */
/**@var \app\models\Client $client */
/**@var \app\models\Recommendation $prev_recommendation */
/**@var \app\models\Work $prev_work */
/**@var \app\models\Order $prev_order */
/**@var array $spta_and_works */

?>

<?php if (isset($atc->apparatus)):?>
    <div class="sl-header-2">
        <p>Перечень проделанных работ и установленных
            ЗиП <?= $atc->apparatus ? ' для ' . $atc->apparatus->getApparatusFullName() : ''; ?></p>
    </div>
    <table class="table table-bordered">
        <thead>
        <tr>
            <th> Номер заявки</th>
            <th> Дата<br>закрытия</th>
            <th> Работа/Деталь</th>
            <th> Кол.</th>
            <th> Счетчик<br>общий</th>
            <th> Счетчик<br>черный</th>
            <th> Счетчик<br>цветной</th>
            <th> Инженер</th>
        </tr>
        </thead>
        <tbody>
        <?php if (isset($spta_and_works['order'])): ?>
            <?php foreach ($spta_and_works['order'] as $order_id => $data): ?>
                <?php
                $span = count($data['data']);
                if (!$span) {
                    $span = 1;
                }

                Yii::info($span, 'test');
                ?>
                <tr>
                    <td rowspan="<?= $span; ?>" style="width: 15px"><?= $order_id ?></td>
                    <td rowspan="<?= $span; ?>"
                        style="width: 15px"><?= Yii::$app->formatter->asDate($data['done_at']) ?></td>
                    <td style="width: 50%"><?= count($data['data']) > 0 ? $data['data'][0]['description'] : '' ?></td>
                    <td style="width: 15px"><?= count($data['data']) > 0 ? $data['data'][0]['num'] : '' ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c1'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c2'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['c3'] ?></td>
                    <td rowspan="<?= $span; ?>"><?= $data['engineer'] ?></td>
                </tr>
                <?php
                if (count($data)) {
                    array_shift($data['data']);
                    Yii::info($data['data'], 'test');
                }
                ?>
                <?php foreach ($data['data'] as $info): ?>
                    <tr>
                        <td style="width: 50%"><?= $info['description'] ?></td>
                        <td style="width: 15px"><?= $info['num'] ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
        <?php endif; ?>
        </tbody>
    </table>
<?php endif; ?>
