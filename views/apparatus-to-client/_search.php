<?php

use app\models\Client;
use johnitvn\ajaxcrud\CrudAsset;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */
/* @var $form yii\widgets\ActiveForm */

CrudAsset::register($this);

?>

<div class="meter-search">

    <?php $form = ActiveForm::begin([
        'action' => ['/apparatus-to-client/info'],
        'method' => 'get',
//        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n{beginWrapper}\n{input}\n{hint}\n{error}\n{endWrapper}",
            'horizontalCssClasses' => [
                'label' => 'col-sm-2',
//                'offset' => 'col-sm-offset-1',
                'wrapper' => 'col-sm-10',
                'error' => '',
                'hint' => '',
            ],
        ],
    ]); ?>

    <div class="row">
        <div class="col-sm-5">
            <?php
            try {
                echo $form->field($model, 'client_id')->widget(\kartik\select2\Select2::class, [
                    'data' => (new Client())->getList(),
                    'options' => [
                        'id' => 'client-dropdown',
                        'prompt' => 'Выберите клиента'
                    ]
                ])->label(false);
            } catch (Exception $e) {
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-sm-3">
            <?php
            try {
                echo $form->field($model, 'id')->widget(\kartik\select2\Select2::class, [
                    'data' => $model->getApparatusListForClient($model->client_id),
                    'options' => [
                        'id' => 'serial-number-dropdown',
                        'prompt' => 'Выберите аппарат',
                    ]
                ])->label(false);
            } catch (Exception $e) {
                echo $e->getMessage();
            } ?>
        </div>
        <div class="col-sm-2">
            <?= Html::submitButton('Показать', [
                'class' => 'btn btn-primary btn-block',
            ]) ?>
        </div>
        <div class="col-sm-2">
            <?= Html::button('Печать',  [
                'id' => 'print-apparatus-info',
                'class' => 'btn btn-info btn-block',
            ]) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

