<?php

use app\models\ApparatusToClient;
use app\models\constants\UsersConstants;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    // [
    // 'class'=>'\kartik\grid\DataColumn',
    // 'attribute'=>'id',
    // ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'apparatus_id',
        'content' => function (ApparatusToClient $model) {
            return $model->apparatus ? $model->apparatus->getApparatusFullName() : null;
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'notes',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
//        'filter' => [0 => 'Джест', 1 => 'Клиент'],
        'attribute' => 'is_owner',
        'content' => function (ApparatusToClient $model) {
            if ($model->is_owner) {
                return 'Клиент';
            }
            return 'Джест';
        }
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'serial_number',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'template' => '{view} {transfer} {update} {delete}',
        'vAlign' => 'middle',
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'transfer' => function ($url) {
                if (UsersConstants::isAdministration()) {
                    return Html::a('<i class="fa fa-arrows-h"></i>', $url,
                        [
                            'title' => 'Передать аппарат',
                            'data-toggle' => 'tooltip',
                            'role' => 'modal-remote',
                        ]);
                }
                return null;
            }
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'View', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Update', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Delete',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
    ],

];   