<?php

/* @var $this yii\web\View */
/* @var $model app\models\ApparatusToClient */
/* @var array $spta_and_works Информация как у СЛ на второй странице, только по всем работам и деталям (без ограничений) */

?>
    <div class="apparatus-to-client-view">

        <div class="row">
            <div class="col-md-12">
                <?= $this->render('_search', ['model' => $model]); ?>

            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <?= $this->render('_apparatus_info', [
                    'spta_and_works' => $spta_and_works,
                    'atc' => $model,
                ]); ?>
            </div>
        </div>


    </div>

<?php
$script = <<<JS
$(document).ready(function() {
    $(document).on('change','#client-dropdown', function() {
        var client_id = $(this).val();
        var sn_dropdown = $('#serial-number-dropdown');
        
         $.get(
            '/apparatus-to-client/get-apparatus-list-for-client',
            {id:client_id},
            function(res) {
                sn_dropdown.html(res)
            }
        );
    });
     $(document).on('click','#print-apparatus-info', function() {
        var atc_id = $('#serial-number-dropdown').val();
       window.open('/apparatus-to-client/print-info?id=' + atc_id, '_blank');
       return true;
    });
});
JS;

$this->registerJs($script);