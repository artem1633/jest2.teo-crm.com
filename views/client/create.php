<?php

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $contract_model app\models\Contract */

?>
<div class="client-create">
    <?= $this->render('_form', [
        'model' => $model,
        'contract_model' => $contract_model,
    ]) ?>
</div>
