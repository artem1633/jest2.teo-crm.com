<?php

use app\models\Client;
use yii\helpers\Html;
use yii\helpers\Url;

return [
//    [
//        'class' => 'kartik\grid\CheckboxColumn',
//        'width' => '20px',
//    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'official_name',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'contact_person',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'phone_number',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'attribute' => 'email',
        'vAlign' => 'middle',
    ],
    [
        'class' => '\kartik\grid\DataColumn',
        'label' => 'Информация',
        'content' => function (Client $model) {
            $app_count = count($model->apparatusToClients ?? []);
            $order_count = $model->ordersCount;
            return Html::a('Аппаратов: ' . $app_count, ['/apparatus-to-client/index', 'client_id' => $model->id], [
                    'role' => 'modal-remote'
                ]) . '<br>' .
                Html::a('Заявок: ' . $order_count, ['/users/index', 'client_id' => $model->id], [
                    'role' => 'modal-remote'
                ]);
        },
        'vAlign' => 'middle',
    ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign' => 'middle',
        'template' => '{add_apparatus} <br> {view} {update} {delete}',
        'urlCreator' => function ($action, $model, $key) {
            return Url::to([$action, 'id' => $key]);
        },
        'buttons' => [
            'add_apparatus' => function ($url, Client $model) {
                return Html::a('<i class="fa fa-plus"></i>', ['/apparatus-to-client/create', 'id' => $model->id], [
                    'title' => 'Добавить аппарат',
                    'data-toggle' => 'tooltip',
                    'role' => 'modal-remote'
                ]);
            },
        ],
        'viewOptions' => ['role' => 'modal-remote', 'title' => 'Просмотр', 'data-toggle' => 'tooltip'],
        'updateOptions' => ['role' => 'modal-remote', 'title' => 'Редактировать', 'data-toggle' => 'tooltip'],
        'deleteOptions' => [
            'role' => 'modal-remote',
            'title' => 'Удалить',
            'data-confirm' => false,
            'data-method' => false,// for overide yii data api
            'data-request-method' => 'post',
            'data-toggle' => 'tooltip',
            'data-confirm-title' => 'Are you sure?',
            'data-confirm-message' => 'Are you sure want to delete this item'
        ],
        'contentOptions' => [
            'style' => 'font-size:1.5rem;'
        ]
    ],

];   