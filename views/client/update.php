<?php

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $file_information array */
/* @var $contract_model \app\models\Contract */
?>
<div class="client-update">

    <?= $this->render('_form', [
        'model' => $model,
        'file_information' => $file_information,
        'contract_model' => $contract_model,
    ]) ?>

</div>
