<?php

use app\models\Contract;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Client */
/* @var $contract_model app\models\Contract */
?>
<div class="panel panel-inverse">
    <div class="panel-heading">
        Информация
    </div>
    <div class="panel-body">
        <?php
        try {
            echo DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'official_name',
                    'address:ntext',
                    'contact_person',
                    'phone_number',
                    'email:email',
//                    [
//                        'attribute' => 'bank_details_file',
//                        'value' => function (Client $model) {
//                            return Html::a(pathinfo($model->bank_details_file)['basename'],
//                                Url::to(['download', 'file' => $model->bank_details_file]));
//                        },
//                        'format' => 'raw'
//                    ],
                ],
            ]);
        } catch (Exception $e) {
            Yii::error($e->getMessage(), '_error');
            echo $e->getMessage();
        } ?>

    </div>
</div>
<?php if ($contract_model): ?>
    <div class="panel panel-inverse">
        <div class="panel-heading">
            Договор
        </div>
        <div class="panel-body">
            <?php
            if ($contract_model) {
                try {
                    echo DetailView::widget([
                        'model' => $contract_model,
                        'attributes' => [
                            'number',
                            'date:date',
                            [
                                'attribute' => 'contract_file',
                                'value' => function (Contract $model) {
                                    return Html::a(pathinfo($model->file)['basename'],
                                        Url::to(['download', 'file' => $model->file]));
                                },
                                'format' => 'raw',
                                'label' => 'Файл договора'
                            ]
                        ],

                    ]);
                } catch (Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                    echo $e->getMessage();
                }
            }
            ?>
        </div>
    </div>
<?php endif; ?>
