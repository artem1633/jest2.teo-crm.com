<?php

use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = "Диспетчеры";
$this->params['breadcrumbs'][] = 'Справочники';
$this->params['breadcrumbs'][] = $this->title;

CrudAsset::register($this);

?>
<div class="panel panel-inverse manager-index">
    <div class="panel-heading">
        <h4 class="panel-title"><?= $this->title; ?></h4>
    </div>
    <div class="panel-body">
        <div id="ajaxCrudDatatable">
            <?php
            try {
                echo GridView::widget([
                    'id' => 'crud-datatable',
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered'],
                    'pjax' => true,
                    'columns' => require(__DIR__ . '/_manager_columns.php'),
                    'toolbar' => [
                        [
                            'content' =>
                                Html::a('<i class="glyphicon glyphicon-plus"></i>', ['create-manager'],
                                    [
                                        'role' => 'modal-remote',
                                        'title' => 'Создать клиента',
                                        'class' => 'btn btn-default'
                                    ]) .
                                '{toggleData}' .
                                '{export}'
                        ],
                    ],
                    'striped' => true,
                    'condensed' => true,
                    'responsive' => true,
                    'panel' => [
                        'headingOptions' => ['style' => 'display: none;'],
                        '<div class="clearfix"></div>',
                    ]
                ]);
            } catch (Exception $e) {
                Yii::error($e->getMessage(), '_error');
                echo $e->getMessage();
            } ?>
        </div>
    </div>
</div>

<?php Modal::begin([
    "id" => "ajaxCrudModal",
    "footer" => "",// always need it for jquery plugin
]) ?>
<?php Modal::end(); ?>
