<?php

use app\models\constants\UsersConstants;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">
 
    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'fio',
                'login',
                [
                    'attribute' => 'open_password',
                    'label' => 'Пароль',
                    'visible' => $model->open_password ? true: false,
                ],
                [
                    'attribute' => 'role',
                    'value' => UsersConstants::getString($model->role)
                ],
            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
