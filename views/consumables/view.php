<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Consumables */
?>
<div class="consumables-view">

    <?php
    try {
        echo DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id',
                'name',
                [
                    'attribute' => 'apparatus_type',
                    'value' => (new \app\models\Apparatus())->getTypeList()[$model->apparatus_type]
                ],

            ],
        ]);
    } catch (Exception $e) {
        Yii::error($e->getMessage(), '_error');
        echo $e->getMessage();
    } ?>

</div>
