<?php

/* @var $this yii\web\View */
/* @var $model app\models\Consumables */
?>
<div class="consumables-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
