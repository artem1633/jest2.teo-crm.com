<?php

/* @var $this yii\web\View */
/* @var $model app\models\Consumables */

?>
<div class="consumables-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
