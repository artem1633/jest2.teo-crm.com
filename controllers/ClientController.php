<?php

namespace app\controllers;

use app\models\Contract;
use app\models\Users;
use Yii;
use app\models\Client;
use app\models\search\ClientSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ClientController implements the CRUD actions for Client model.
 */
class ClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Client models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ClientSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Client model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $contract_model = $model->contract ?? null;

        if ($contract_model) {
            Yii::info($contract_model->attributes, 'test');
        }

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => $model->official_name,
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                    'contract_model' => $contract_model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
                'contract_model' => $contract_model,
            ]);
        }
    }

    /**
     * Creates a new Client model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $request = Yii::$app->request;
        $model = new Client();
        $contract_model = new Contract();
        $caller = $request->get('caller');

        Yii::warning($caller, 'test');

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Создание нового клиента",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                        'contract_model' => $contract_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    $model->uploadBankDetails();
                    if ($contract_model->load($request->post())) {
                        $contract_model->client_id = $model->id;
                        if (!$contract_model->save()) {
                            Yii::error($contract_model->errors, '_error');
                        }
                        $contract_model->uploadBankDetails();
                    }

                    return [
                        'forceReload' => $caller ? '': '#crud-datatable-pjax',
                        'size' => 'md',
                        'title' => "Создание нового клиента",
                        'content' => '<span class="text-success">Клиент создан успешно</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            ($caller != null ?
                                Html::a('Вернуться к заявке', [$caller],
                                    ['class' => 'btn btn-primary']) :
                                Html::a('Создать ещё', ['create'],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']))

                    ];
                } else {
                    return [
                        'title' => "Создание нового клиента",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                            'contract_model' => $contract_model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                    'contract_model' => $contract_model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Client model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $contract_model = $model->contract;

        if (!$contract_model) {
            $contract_model = new Contract();
        }

        Yii::info($contract_model->attributes, 'test');

        $file_information = [
            'path' => $contract_model->file,
            'caption' => pathinfo($contract_model->file)['basename'],
            'size' => filesize($contract_model->file)
        ];

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {

                return [
                    'title' => 'Редактирование ' . $model->official_name,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'file_information' => $file_information,
                        'contract_model' => $contract_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    Yii::info($model->attributes, 'test');
                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                    }
                    if ($contract_model->load($request->post())) {
                        $contract_model->client_id = $model->id;
                        if (!$contract_model->save()) {
                            Yii::error($contract_model->errors, '_error');
                        }
                        $contract_model->uploadBankDetails();
                    }

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => $model->official_name,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                            'contract_model' => $contract_model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Редактировать', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => 'Редактирование ' . $model->official_name,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'contract_model' => $contract_model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Client model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Client model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Client model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Client the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Client::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionDownload($file)
    {
        Yii::$app->response->sendFile($file);
    }

//    /**
//     * @param int $id Идентификатор клиента, для которого создается пользователь
//     * @return array
//     */
//    public function actionCreateUser($id)
//    {
//        $client = Client::findOne($id) ?? null;
//
//        if ($client) {
//            Yii::$app->response->format = Response::FORMAT_JSON;
//
//            /** @var Users $user */
//            $user = $client->createUser();
//            if ($user) {
//                return [
//                    'title' => 'Пользователь создан',
//                    'forceReload' => '#crud-datatable-pjax',
//                    'content' => $this->renderAjax('@app/views/users/view', ['model' => $user]),
//                    'footer' =>
//                        Html::button('Сохранить',
//                            [
//                                'class' => 'btn btn-info pull-left',
//                                'onClick' => "
//                                    var pass = $user->open_password;
//                                    $.post(
//                                        '/client/save-user-info',
//                                        {
//                                            id: $user->id,
//                                            password: pass
//                                        }
//                                    )
//                                "
//                            ])
//                        . Html::button('Ок', [
//                            'class' => 'btn btn-success',
//                            'data-dismiss' => 'modal'
//                        ])
//                ];
//            }
//
//        }
//        return [
//            'title' => 'Ошибка создания пользователя',
//            'content' => '<h3 class="alert-danger">Ошибка. Клиент не найден!</h3>',
//            'footer' => Html::button('Закрыть', [
//                'class' => 'btn btn-info',
//                'data-dismiss' => 'modal'
//            ])
//        ];
//    }

    public function actionSaveUserInfo()
    {
        $request = Yii::$app->request;

        if (!$request->isPost) {
            return false;
        }

        $id = $request->post('id');
        $pass = $request->post('password');

        $user = Users::findOne($id);
        $user->open_password = $pass;

        $file_path = $user->saveInfo();

        return $this->redirect(['send-file', 'file' => $file_path]);
    }

    public function actionSendFile($file)
    {
        Yii::$app->response->sendFile($file);
    }

}
