<?php

namespace app\controllers;

use app\models\Apparatus;
use app\models\constants\UsersConstants;
use app\models\User;
use app\models\Users;
use Yii;
use app\models\Report;
use app\models\search\ReportSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ReportController implements the CRUD actions for Report model.
 */
class ReportController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Report models.
     * @return mixed
     */
    public function actionIndex()
    {
        /** @var User $identity */
        $identity = Yii::$app->user->identity;

        $searchModel = new ReportSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        if (UsersConstants::isSuperAdmin()) {
            $dataProvider->query->andWhere(['<>', 'status', Report::STATUS_DRAFT]);
            $dataProvider->query->orderBy(['status' => SORT_ASC, 'sented_at' => SORT_DESC]);
        } else {
            $dataProvider->query->andWhere(['client.id' => $identity->client_id]);
            $dataProvider->query->orderBy(['sented_at' => SORT_DESC]);
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Report model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Отчет " . $model->client->official_name . '. ' . $model->getMonthName($model->report_month) . '. ' . $model->report_year . 'г.',
                'content' => $this->renderAjax('view', [
                    'model' => $model,
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Creates a new Report model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws \yii\db\Exception
     */
    public function actionCreate()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        $request = Yii::$app->request;
        $model = new Report();
        $model->client_id = $identity->client_id;

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Добавление отчета",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Отправить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post())) {

                    $model->prepareSave();

                    Yii::info($model->attributes, 'test');

                    if (!$model->checkFile()) {
                        $model->addError('photo', 'Ошибка загрузка фото счетчика/ов');
                        return [
                            'title' => "Добавление отчета",
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }

                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                        return [
                            'title' => "Добавление отчета",
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }
                    $model->uploadFile();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Подтверждение отправки информации",
                        'content' => 'Подтверждаете отправку информации?',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Подтвердить', ['change-status', 'id' => $model->id, 'status' => $model::STATUS_NEW],
                                ['class' => 'btn btn-success', 'role' => 'modal-remote'])

                    ];

//                    return [
//                        'forceReload' => '#crud-datatable-pjax',
//                        'title' => "Добавление отчета",
//                        'content' => '<span class="text-success">Create Report success</span>',
//                        'footer' => Html::button('Закрыть',
//                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
//                            Html::a('Еще отчет', ['create'], ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
//
//                    ];
                } else {
                    return [
                        'title' => "Добавление отчета",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

    }

    /**
     * Updates an existing Report model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws \yii\db\Exception
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $model->apparatus_id = $model->apparatusToClient->apparatus_id;

        if ($model->status >= $model::STATUS_APPROVED && !UsersConstants::isSuperAdmin()) {
            //Если отчет утвержден или отклонен
            throw new ForbiddenHttpException('Показания счтечика уже утверждены, изменение невозможно!');
        }

        if ($model->apparatus->type = Apparatus::TYPE_COLORED) {
            $model->counter_3 = $model->counter_1;
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Редактирование отчета" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    $model->prepareSave();
                    if (!$model->checkFile()) {
                        $model->addError('photo', 'Ошибка загрузка фото счетчика/ов');
                        return [
                            'title' => "Добавление отчета",
                            'content' => $this->renderAjax('create', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }
                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                        return [
                            'title' => "Редактирование отчета" . $id,
                            'content' => $this->renderAjax('update', [
                                'model' => $model,
                            ]),
                            'footer' => Html::button('Закрыть',
                                    ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                                Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                        ];
                    }
                    $model->uploadFile();
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Отчет " . $model->client->official_name . '. ' . $model->getMonthName($model->report_month) . '. ' . $model->report_year . 'г.',
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Редактировать', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Редактирование отчета " . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing Report model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Report model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Report model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Report the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Report::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Меняет статусы для отчета
     * @param integer $id Идентификатор отчета
     * @param integer $status Статус
     * @return array|Response
     * @throws \HttpInvalidParamException
     */
    public function actionChangeStatus($id, $status)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $report = Report::findOne($id) ?? null;

        if (!$report) {
            return [
                'title' => 'Ошибка',
                'content' => 'Отчет не найден'
            ];
        }

        $report->status = $status;

        if (!$report->save(false)) {
            Yii::error($report->errors, '_error');
        }
        if ($report->status == $report::STATUS_NEW) {
            $report->sendNotify();
            return [
                'forceReload' => '#crud-datatable-pjax',
                'title' => 'Отправка отчета',
                'content' => 'Отчет отправлен успешно.',
                'footer' => Html::button('Закрыть', [
                    'class' => 'btn btn-default pull-right',
                    'data-dismiss' => "modal"
                ])
            ];
        }

        return [
            'forceReload' => '#crud-datatable-pjax',
            'forceClose' => true,
            'footer' => Html::button('Закрыть', [
                'class' => 'btn btn-default pull-right',
                'data-dismiss' => "modal"
            ])
        ];
    }

}
