<?php

namespace app\controllers;

use app\models\Condition;
use app\models\search\ComplaintSearch;
use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Yii;
use app\models\Order;
use app\models\search\OrderSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $request = Yii::$app->request;

        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->query->andWhere(['<>', 'status', Order::STATUS_CANCEL]);

        if ($request->get('append')) {
            $append = '(' . $request->get('append') . ')';
        } else {
            $append = '';
        }

        Url::remember();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'append' => $append,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "Order #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new Order model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();
        $model->status = $model::STATUS_NEW;

        if (!$model->save()) {
            Yii::error($model->errors, '_error');
        }

        return $this->redirect(['/order/update', 'id' => $model->id]);
    }

    /**
     * Updates an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);
        $condition_model = $model->condition;

        if (!$condition_model) {
            $condition_model = new Condition();
        }
        $complaintSearchModel = new ComplaintSearch();
        $complaintDataProvider = $complaintSearchModel->search(Yii::$app->request->queryParams);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Заявка №" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                        'complaintSearchModel' => $complaintSearchModel,
                        'complaintDataProvider' => $complaintDataProvider,
                        'condition_model' => $condition_model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post())) {
                    if ($model->apparatus_client_id && $model->testOfRepeat()) {
                        $model->is_repeat = 1;
                    }

                    if (!$model->save()) {
                        Yii::error($model->errors, '_error');
                    }
                    $condition_model->load($request->post());
                    $condition_model->order_id = $model->id;
                    $condition_model->save();

                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Order #" . $id,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                            'condition_model' => $condition_model,
                            'complaintSearchModel' => $complaintSearchModel,
                            'complaintDataProvider' => $complaintDataProvider,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Редактировать', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Update Order #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                            'condition_model' => $condition_model,
                            'complaintSearchModel' => $complaintSearchModel,
                            'complaintDataProvider' => $complaintDataProvider,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post())) {
                if ($model->apparatus_client_id && $model->testOfRepeat()) {
                    $model->is_repeat = 1;
                }

                if (!$model->save()) {
                    Yii::error($model->errors, '_error');
                }

                $condition_model->load($request->post());
                $condition_model->order_id = $model->id;
                $condition_model->save();

                return $this->redirect([
                    '/order',
                    'OrderSearch[status]' => $model::STATUS_CURRENT,
                    'append' => 'Текущие'
                ]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                    'condition_model' => $condition_model,
                    'complaintSearchModel' => $complaintSearchModel,
                    'complaintDataProvider' => $complaintDataProvider,
                ]);
            }
        }
    }

    /**
     * Delete an existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
//        $this->findModel($id)->delete();
        $order = $this->findModel($id);
        $order->status = $order::STATUS_CANCEL;
        $order->save(false);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing Order model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Отдает сформированный сервисный лист
     * @param int $id Идентификатор заявки
     * @throws \Mpdf\MpdfException
     */
    public function actionGetSl($id)
    {
        $order = Order::findOne($id);

        //Выставляем заявке статус в работе
        $order->status = $order::STATUS_WORK;
        $order->save();


        $prev_order = $order->getPrevOrder();
        $apparatus_to_client = $order->apparatusToClient;
        $client = $order->client;
        $prev_recommendation = $order->getSLRecommendation();
//        Yii::warning($prev_recommendation, 'test');
        $prev_work = $order->getLastWork();
//        $all_spta = $order->apparatusToClient->getAllSpta(Spta::TYPE_INSTALL);
//        $all_works = $order->apparatusToClient->getAllWorks(Work::STATUS_FINISH);

        $spta_and_works = $order->getSptaAndWorksForPrevOrder();


        $mpdf = new Mpdf([
            'mode' => 'utf-8',
        ]);
        $mpdf->WriteHTML(file_get_contents(Url::to(['/css/sl_style.css'], true)), HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($this->renderPartial('_service_list', [
            'order' => $order,
            'atc' => $apparatus_to_client,
            'client' => $client,
            'prev_recommendation' => $prev_recommendation,
            'prev_work' => $prev_work,
//            'all_spta' => $all_spta,
//            'all_works' => $all_works,
            'prev_order' => $prev_order,
            'spta_and_works' => $spta_and_works,
        ]), HTMLParserMode::HTML_BODY);

        $mpdf->Output();

    }

    /**
     * Завершает заявку
     * @param int $id Идентификатор заявки
     * @return array|Response
     */
    public function actionFinish($id)
    {
        $request = Yii::$app->request;

        $order = Order::findOne($id);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;

            if ($order->load($request->post())) {
                $order->status = $order::STATUS_DONE;
                $order->save();
                return [
                    'forceReload' => '#crud-datatable-pjax',
                    'forceClose' => true,
                ];
            } else {
                return [
                    'title' => 'Укажите дату закрытия заявки',
                    'content' => $this->renderAjax('_close_form', [
                        'model' => $order
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::submitButton('Закрыть заявку', ['class' => 'btn btn-primary'])
                ];
            }
        }

        return $this->redirect('index');
    }

    public function actionSetClient()
    {
        $request = Yii::$app->request;

        Yii::$app->response->format = Response::FORMAT_JSON;

        $order_id = $request->post('order_id');
        $client_id = $request->post('client_id');

        $order = Order::findOne($order_id);

        if ($order) {
            $order->client_id = $client_id;
            if (!$order->save()) {
                Yii::error($order->errors, '_error');
            }
        }
    }

    /**
     * КОпирует заявку на основнаии текущей
     * @param int $id Идентификатор заявки
     * @return Response
     */
    public function actionCopyOrder($id)
    {
        $target_order = Order::findOne($id);
        $order = new Order([
                'client_id' => $target_order->client_id,
                'apparatus_client_id' => $target_order->apparatus_client_id,
            ]
        );

        if (!$order->save()){
            Yii::error($order->errors, '_error');
            return  $this->redirect(['index', 'OrderSearch[status]'=>5, 'append' => 'Текущие']);
        }

        return $this->redirect(['/order/update', 'id' => $order->id]);

    }
}
