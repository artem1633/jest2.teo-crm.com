<?php

namespace app\controllers;

use app\models\Client;
use Mpdf\HTMLParserMode;
use Mpdf\Mpdf;
use Yii;
use app\models\ApparatusToClient;
use app\models\search\ApparatusToClientSearch;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \yii\web\Response;
use yii\helpers\Html;

/**
 * ApparatusToClientController implements the CRUD actions for ApparatusToClient model.
 */
class ApparatusToClientController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'bulk-delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ApparatusToClient models.
     * @param int $client_id Идентификатор клинета
     * @return mixed
     */
    public function actionIndex($client_id)
    {
        $request = Yii::$app->request;

        $client = Client::findOne($client_id);

        $searchModel = new ApparatusToClientSearch();

//        Yii::info(Yii::$app->request->queryParams, 'test');

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        $dataProvider->query->andFilterWhere(['client_id' => $client->id]);

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'size' => 'large',
                'title' => 'Аппараты. ' . $client->official_name,
                'content' => $this->renderAjax('index', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
                ])
            ];
        }
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ApparatusToClient model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $request = Yii::$app->request;
        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                'title' => "ApparatusToClient #" . $id,
                'content' => $this->renderAjax('view', [
                    'model' => $this->findModel($id),
                ]),
                'footer' => Html::button('Закрыть',
                        ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                    Html::a('Редактировать', ['update', 'id' => $id],
                        ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
            ];
        } else {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
    }

    /**
     * Creates a new ApparatusToClient model.
     * For ajax request will return json object
     * and for non-ajax request if creation is successful, the browser will be redirected to the 'view' page.
     * @param int $id Идентификатор клиента
     * @param string $pjaxReloadContainer Контейнер для перезагрузки
     * @return mixed
     */
    public function actionCreate($id, $pjaxReloadContainer = 'order-pjax-container')
    {
        $request = Yii::$app->request;
        $model = new ApparatusToClient();
        $model->client_id = $id;
        $caller = $request->get('caller');

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                $model->is_owner = 1;
                return [
                    'size' => 'large',
                    'title' => "Добавление аппарата клиенту",
                    'content' => $this->renderAjax('create', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    if ($caller) {
                        return [
                            'forceReload' => '#' . $pjaxReloadContainer,
                            'forceClose' => true,
                        ];
                    }
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "Добавление аппарата клиенту",
                        'content' => '<span class="text-success">Аппарат добавлен успешно</span>',
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            ($caller != null ?
                                Html::a('Вернуться к заявке', [$caller],
                                    ['class' => 'btn btn-primary']) :
                                Html::a('Создать ещё', ['create', 'id' => $model->client_id],
                                    ['class' => 'btn btn-primary', 'role' => 'modal-remote']))
                    ];
                } else {
                    return [
                        'title' => "Добавление аппарата клиенту",
                        'content' => $this->renderAjax('create', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])

                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Updates an existing ApparatusToClient model.
     * For ajax request will return json object
     * and for non-ajax request if update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $request = Yii::$app->request;
        $model = $this->findModel($id);

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => "Update ApparatusToClient #" . $id,
                    'content' => $this->renderAjax('update', [
                        'model' => $model,
                    ]),
                    'footer' => Html::button('Закрыть',
                            ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                        Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                ];
            } else {
                if ($model->load($request->post()) && $model->save()) {
                    return [
                        'forceReload' => '#crud-datatable-pjax',
                        'title' => "ApparatusToClient #" . $id,
                        'content' => $this->renderAjax('view', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::a('Редактировать', ['update', 'id' => $id],
                                ['class' => 'btn btn-primary', 'role' => 'modal-remote'])
                    ];
                } else {
                    return [
                        'title' => "Update ApparatusToClient #" . $id,
                        'content' => $this->renderAjax('update', [
                            'model' => $model,
                        ]),
                        'footer' => Html::button('Закрыть',
                                ['class' => 'btn btn-default pull-left', 'data-dismiss' => "modal"]) .
                            Html::button('Сохранить', ['class' => 'btn btn-primary', 'type' => "submit"])
                    ];
                }
            }
        } else {
            /*
            *   Process for non-ajax request
            */
            if ($model->load($request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Delete an existing ApparatusToClient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $request = Yii::$app->request;
        $this->findModel($id)->delete();

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }


    }

    /**
     * Delete multiple existing ApparatusToClient model.
     * For ajax request will return json object
     * and for non-ajax request if deletion is successful, the browser will be redirected to the 'index' page.
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionBulkDelete()
    {
        $request = Yii::$app->request;
        $pks = explode(',', $request->post('pks')); // Array or selected records primary keys
        foreach ($pks as $pk) {
            $model = $this->findModel($pk);
            $model->delete();
        }

        if ($request->isAjax) {
            /*
            *   Process for ajax request
            */
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ['forceClose' => true, 'forceReload' => '#crud-datatable-pjax'];
        } else {
            /*
            *   Process for non-ajax request
            */
            return $this->redirect(['index']);
        }

    }

    /**
     * Finds the ApparatusToClient model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ApparatusToClient the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ApparatusToClient::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Возвращает аппараты для клиента
     * @param $id
     * @return array
     */
    public function actionGetApparatusForClient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $data = '';

        foreach ((new ApparatusToClient)->getApparatusListForClient($id) as $apparatus_id => $apparatus_name) {
            $data .= "<option value='{$apparatus_id}'>{$apparatus_name}</option>";
        }

        return [
            'success' => 1,
            'data' => $data,
        ];
    }

    public function actionInfo()
    {
        $request = Yii::$app->request;
        $model = new ApparatusToClient();
        $client_id = null;
        $spta_data = [];

        if (isset($request->queryParams['ApparatusToClient']['id'])) {
            $atc_id = $request->queryParams['ApparatusToClient']['id'];

            $model = $model::findOne($atc_id);

            Yii::info($request->queryParams, 'test');
            $spta_data = $model->getSptaAndWorks();
        }


        return $this->render('info', [
            'model' => $model,
            'spta_and_works' => $spta_data,
            'atc' => $model,
        ]);
    }

    /**
     * Получает серийники для аппаратов клиента
     * @param int $id Идентификатор клиента
     * @return string
     */
    public function actionGetSnForClient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = ApparatusToClient::find()->andWhere(['client_id' => $id]);

        $options = '';

        /** @var ApparatusToClient $atc */
        foreach ($query->each() as $atc) {
            $options .= "<option value='{$atc->id}'>{$atc->serial_number}</option>";
        }

        return $options;
    }

    /**
     * Возвращает список аппаратов для клиента Марка + Модель + с/н
     * @param int $id Идентификатор клиента
     * @return string
     */
    public function actionGetApparatusListForClient($id)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $query = ApparatusToClient::find()->andWhere(['client_id' => $id]);

        $options = '';

        /** @var ApparatusToClient $atc */
        foreach ($query->each() as $atc) {
            if ($atc->apparatus){
                $brand = $atc->apparatus->brand ? $atc->apparatus->brand->name : 'No brand';
                $model = $atc->apparatus->model;
            } else {
                $brand = 'No brand';
                $model = 'No model';
            }
            $options .= "<option value='{$atc->id}'>{$brand}&nbsp;{$model}&nbsp;№{$atc->serial_number}</option>";
        }

        return $options;
    }

    /**
     * Вывод в PDF информации о ЗиП и работах для аппарата клиента
     * @param int $id Идентификатор аппарата клиента (apparatus_to_client)
     * @throws \Mpdf\MpdfException
     */
    public function actionPrintInfo($id)
    {
        $atc = ApparatusToClient::findOne($id);
        $spta_and_works = $atc->getSptaAndWorks();

        $mpdf = new Mpdf([
            'mode' => 'utf-8',
        ]);
        $mpdf->WriteHTML(file_get_contents(Url::to(['/css/sl_style.css'], true)), HTMLParserMode::HEADER_CSS);
        $mpdf->WriteHTML($this->renderPartial('_apparatus_info', [
            'atc' => $atc,
            'spta_and_works' => $spta_and_works,
        ]), HTMLParserMode::HTML_BODY);

        $mpdf->Output();
    }

    /**
     * Передача аппарата от клиента клиенту
     * @param int $id Идентификатор аппарата клиента (apparatus_to_client)
     * @return array
     */
    public function actionTransfer($id)
    {
        $request = Yii::$app->request;
        $model = ApparatusToClient::findOne($id);
        $model->owner = $model->client->official_name;

        if ($request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            if ($request->isGet) {
                return [
                    'title' => 'Передача аппарата. ' . $model->apparatus->getApparatusFullName() . ' №' . $model->serial_number,
                    'content' => $this->renderAjax('_transfer_form', [
                        'model' => $model
                    ]),
                    'footer' => Html::submitButton('Передать', [
                        'class' => 'btn btn-info',
                    ])
                ];
            } else {
                if ($model->load($request->post())) {
                    Yii::info($model->new_owner_id, 'test');
                    if ($model->new_owner_id) {
                        $model->client_id = $model->new_owner_id;
                        if (!$model->save()) {
                            Yii::error($model->errors, '_error');
                        }
                        return [
                            'forceReload' => '#crud-datatable-pjax',
                            'forceClose' => true,
                        ];
                    }
                }
            }

        }
    }
}
