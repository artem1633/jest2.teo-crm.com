<?php

namespace app\components;

use app\models\Settings;
use HttpInvalidParamException;
use Yii;

/**
 * @property string $recipients Получатели сообщения (разделитель запятая)
 * @property array $recipients_arr Получатели сообщения
 * @property string $sender_email Email Отправителя сообщения
 * @property string $sender_name Имя Отправителя сообщения
 * @property int $email_type Тип сообщения
 * @property string $html_body Тело сообщения
 * @property string $subject Тема сообщения
 * @property string $errors Ошибки
 */
class EmailHelper
{
    const TYPE_ORDER = 1;
    const TYPE_REPAIRING = 2;
    const TYPE_REPORT = 3;
    const TYPE_CLIENT = 4;
    const TYPE_REPEAT = 5;

    public $recipients;
    private $recipients_arr;
    public $sender_email;
    public $sender_name;
    public $email_type;
    public $html_body;
    public $subject;
    public $errors;

    public function __construct()
    {
        $this->sender_email = Yii::$app->params['adminEmail'];
        $this->sender_name = 'Джест CMS';
    }

    /**
     * Отправляет сообщение получателям
     * @return bool
     * @throws HttpInvalidParamException
     */
    public function send()
    {
        $this->setRecipients();
        $this->errors = [];

        if (!$this->sender_email || !$this->subject || !$this->html_body) {
            throw new HttpInvalidParamException('Недостаточно параметров');
        }

        if (!$this->sender_name) $this->sender_name = $this->sender_email;

        foreach ($this->recipients_arr as $email) {
            try {
                Yii::$app->mailer->compose()
                    ->setFrom([$this->sender_email => $this->sender_name])
                    ->setTo(trim($email))
                    ->setSubject($this->subject)
                    ->setHtmlBody($this->html_body)
                    ->send();
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), '_error');
                $this->errors[] = $e->getMessage();
            }
            sleep(1);
        }

        if ($this->errors){
            return false;
        }
        return true;
    }

    /**
     * Определяет получателей сообщения
     * @throws HttpInvalidParamException
     */
    protected function setRecipients()
    {
        switch ($this->email_type) {
            case self::TYPE_ORDER:
                $this->recipients_arr = explode(',', (new Settings)->getValueByKey('order_recipient'));
                break;
            case self::TYPE_REPAIRING:
                $this->recipients_arr = explode(',', (new Settings)->getValueByKey('repairing_recipient'));
                break;
            case self::TYPE_REPORT:
                $this->recipients_arr = explode(',', (new Settings)->getValueByKey('report_recipient'));
                break;
            case self::TYPE_REPEAT:
                $this->recipients_arr = explode(',', (new Settings)->getValueByKey('repeat_order_notify_emails'));
                break;
            default:
                $this->recipients_arr = explode(',', $this->recipients);
        }

        if (!$this->recipients_arr) {
            throw new HttpInvalidParamException('Не найдены получатели сообщения');
        }
    }
}