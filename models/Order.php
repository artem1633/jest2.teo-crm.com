<?php

namespace app\models;

use app\components\EmailHelper;
use app\models\constants\UsersConstants;
use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $created_at
 * @property int $client_id Клиент
 * @property int $counter_1 Счетчик 1
 * @property int $counter_2 Счетчик 2
 * @property int $counter_3 Счетчик 3
 * @property string $photos Путь к папке с фотками счетчиков
 * @property string $done_at Дата закрытия заявки
 * @property int $status Статус
 * @property int $apparatus_client_id Аппарат клиента
 * @property int $service_type Тип услуг (Выезд/Стационар)
 * @property int $work_type Тип работ (Гарантия/Подготовка/Платный/Контракт)
 * @property int $parent_order_id Родительская заявка
 * @property int $total_time Затраченное время (час.)
 * @property string $account_number Номер счета
 * @property int $manager_id Диспетчер
 * @property int $engineer_id Инженер
 * @property int $is_repeat Повторная заявка
 * @property string $serial_number Серийный номер аппарата
 * @property string $client_name Название клиента
 * @property string $apparatus_name Марка + модель аппарата
 * @property string $search_start_date Начальная дата поиска
 * @property string $search_end_date КОнечная дата поиска
 * @property int $search_type_date Искать по дате создания (0) или по дате закрытия заявки (1)
 * @property string $a_condition Состояние аппарата для текущей заявки
 * @property string $engineer_fio ФИО инженера
 *
 * @property ApparatusToClient $apparatusToClient
 * @property Client $client
 * @property Users $engineer
 * @property Users $manager
 * @property Order $parentOrder
 * @property Order[] $orders
 * @property OrderToConsumables[] $orderToConsumables
 * @property string $statusName Название статуса
 * @property Complaint[] $complaints Жалобы
 * @property string $serviceTypeLabel
 * @property string $workTypeLabel
 * @property Apparatus $apparatus
 * @property Recommendation[] $recommendations
 * @property Work[] $works
 * @property Spta[] $sptas ЗИП для заявки
 * @property Condition $condition Состояние аппарата
 */
class Order extends ActiveRecord
{
    //Статуы Новая/В работе/Выпонена/Отменена
    const STATUS_NEW = 1;
    const STATUS_WORK = 2;
    const STATUS_DONE = 3;
    const STATUS_CANCEL = 4;
    const STATUS_CURRENT = 5; //Только для поиска по заявкам сюда входят STATUS_NEW и STATUS_WORK

    //Тип услуг (Выезд/Стационар)
    const SERVICE_TYPE_VISIT = 1;
    const SERVICE_TYPE_STATIONARY = 2;

    //Тип работ (Гарантия/Подготовка/Платный/Контракт)
    const WORK_TYPE_GUARANTEE = 1;
    const WORK_TYPE_PREPARE = 2;
    const WORK_TYPE_PAID = 3;
    const WORK_TYPE_CONTRACT = 4;

    public $serial_number;
    public $client_name;
    public $apparatus_name;
    public $a_condition;

    public $search_start_date;
    public $search_end_date;
    public $search_type_date;

    const SEARCH_CREATE_DATE = 0;
    const SEARCH_DONE_DATE = 1;

    public $need_send_email = false;
    public $engineer_fio;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'done_at'], 'safe'],
            [
                [
                    'client_id',
                    'counter_1',
                    'counter_2',
                    'counter_3',
                    'status',
                    'apparatus_client_id',
                    'service_type',
                    'work_type',
                    'parent_order_id',
                    'total_time',
                    'manager_id',
                    'engineer_id',
                    'is_repeat'
                ],
                'integer'
            ],
            [['photos', 'account_number'], 'string', 'max' => 255],
            [
                ['apparatus_client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ApparatusToClient::className(),
                'targetAttribute' => ['apparatus_client_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [
                ['engineer_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['engineer_id' => 'id']
            ],
            [
                ['manager_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Users::className(),
                'targetAttribute' => ['manager_id' => 'id']
            ],
            [
                ['parent_order_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Order::className(),
                'targetAttribute' => ['parent_order_id' => 'id']
            ],
            ['serial_number', 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '#',
            'created_at' => 'Дата создания',
            'client_id' => 'Заказчик',
            'counter_1' => 'Счетчик общий',
            'counter_2' => 'Счетчик ч/б',
            'counter_3' => 'Счетчик цвет',
            'photos' => 'Путь к папке с фотками счетчиков',
            'done_at' => 'Дата закрытия заявки',
            'status' => 'Статус',
            'apparatus_client_id' => 'Аппарат заказчика',
            'service_type' => 'Тип услуг', // (Выезд/Стационар)
            'work_type' => 'Тип работ', // (Гарантия/Подготовка/Платный/Контракт)
            'parent_order_id' => 'Родительская заявка',
            'total_time' => 'Затраченное время (час.)',
            'account_number' => 'Номер счета',
            'manager_id' => 'Диспетчер',
            'engineer_id' => 'Инженер',
            'is_repeat' => 'Повторная заявка',
            'engineer_fio' => 'Инженер',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->created_at) {
            $this->created_at = date('Y-m-d H:i:s', time());
        }

        if (!$insert){
            $current_repeat = Order::findOne($this->id)->is_repeat;
            if ($this->is_repeat != $current_repeat && $this->is_repeat == 1) {
                //Если заявка только что стала повторной - ставим флаг на отправку оповещения
                $this->need_send_email = true;
            }
            if ($this->isAttributeChanged('done_at')) {
                //Если указана дата закрытия заявки, выставляем статус "Закрыта"
                if ($this->done_at) {
                    $this->status = $this::STATUS_DONE;
                }
            }
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->need_send_email) {
            //Отправляем письмо о повторной заявке
            $email = new EmailHelper();
            $email->email_type = $email::TYPE_REPEAT;
            $email->sender_name = 'База СЦ ДЖЕСТ';
            $email->subject = 'Внимание! Повторная заявка!';
            $email->html_body = '&nbsp;';
            try {
                $result = $email->send();
                Yii::info('Email sent: ' . (string)$result, 'test');
            } catch (\HttpInvalidParamException $e) {
                Yii::error($e->getMessage(), '_error');
            }
        } else {
            Yii::info('Skip sending email', 'test');
        }


    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::class, ['id' => 'apparatus_id'])
            ->via('apparatusToClient');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id'])
            ->via('apparatus');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClient()
    {
        return $this->hasOne(ApparatusToClient::className(), ['id' => 'apparatus_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComplaints()
    {
        return $this->hasMany(Complaint::class, ['order_id' => 'id'])->andWhere(['order_id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEngineer()
    {
        return $this->hasOne(Users::className(), ['id' => 'engineer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getManager()
    {
        return $this->hasOne(Users::className(), ['id' => 'manager_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'parent_order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['parent_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToConsumables()
    {
        return $this->hasMany(OrderToConsumables::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConditions()
    {
        return $this->hasMany(Condition::class, ['order_id' => 'id'])->andWhere(['order_id' => $this->id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCondition()
    {
        return $this->hasOne(Condition::class, ['order_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\OrderQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\OrderQuery(get_called_class());
    }

    /**
     * Возвращает ярлычок с количеством новых заявок и заявок в работе
     * @return string
     */
    public function getLabelCounts()
    {
        if (!UsersConstants::isSuperAdmin()) {
            return '';
        }

        $new_orders = self::find()->andWhere([
            'OR',
            ['status' => self::STATUS_NEW],
            ['status' => self::STATUS_WORK]
        ])->count();

        if ($new_orders > 0) {
            return "<span class='label label-warning' style='margin-left: 5px;' title='Новые заказы'>{$new_orders}</span>";
        }

        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecommendations()
    {
        return $this->hasMany(Recommendation::class, ['order_id' => 'id']);

    }

    /**
     * ЗИП для заявки
     * @return \yii\db\ActiveQuery
     */
    public function getSptas()
    {
        return $this->hasMany(Spta::class, ['order_id' => 'id']);
    }

    /**
     * Массив статусов и наименований
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_NEW => 'Новая',
            self::STATUS_WORK => 'В работе',
            self::STATUS_DONE => 'Выполнена',
//            self::STATUS_CANCEL => 'Отменена',
        ];
    }

    /**
     * Массив типов услуг и наименований
     * @return array
     */
    public function getServiceTypeList()
    {
        return [
            self::SERVICE_TYPE_VISIT => 'Выезд',
            self::SERVICE_TYPE_STATIONARY => 'Стационар',
        ];
    }

    /**
     * Массив типов работ и наименований
     * @return array
     */
    public function getTypeWorkList()
    {
        return [
            self::WORK_TYPE_GUARANTEE => 'Гарантия',
            self::WORK_TYPE_PREPARE => 'Подготовка',
            self::WORK_TYPE_PAID => 'Платный',
            self::WORK_TYPE_CONTRACT => 'Контракт',
        ];
    }

    /**
     * Наименование типа обслуживания
     * @return mixed
     */
    public function getServiceTypeLabel()
    {
        if ($this->service_type) {
            return self::getServiceTypeList()[$this->service_type];
        }
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorks()
    {
        return $this->hasMany(Work::class, ['order_id' => 'id']);
    }

    /**
     * Наименование типа работ
     * @return mixed
     */
    public function getWorkTypeLabel()
    {
        if ($this->work_type) {
            return self::getTypeWorkList()[$this->work_type];
        }

        return null;
    }

    /**
     * Загрузка фоток счетчиков
     * @return bool
     * @throws \yii\base\Exception
     */
    public function uploadCountersPhoto()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_IMAGE;

        $target_dir = 'uploads/counters/order/' . $this->client_id;

        //=======================//
        $target_name = Yii::$app->security->generateRandomString(10);

        Yii::info('Целевая папка: ' . $target_dir, 'test');

        $form->file = UploadedFile::getInstance($this, 'imageFile');
        Yii::info($form->file, 'test');

        if ($form->file) {
            Yii::info('Имя файла: ' . $target_name, 'test');
            $path_file = $form->upload($target_dir, $target_name);
        } else {
            Yii::info('Файл не загружен', 'test');
            return false;
        }
        //============//

//        $ext = pathinfo($_FILES['Order']['name']['image'][0], PATHINFO_EXTENSION);
//        $target_name = Yii::$app->security->generateRandomString(10) . '.' . $ext;
//        $path_file = $target_dir . '/' . $target_name;
//        move_uploaded_file($_FILES['Order']['tmp_name']['image'][0], $path_file);

        Yii::info($path_file, 'test');

        $this->photos = $path_file;

        return true;
    }

    /**
     * Возвращает наименование статуса по ID
     * @param int $status Статус
     * @return string
     */
    public function getStatusName()
    {
        return $this->getStatusList()[$this->status];
    }

    /**
     * Возвращает последнюю рекомендацию для аппарата
     * @return Recommendation
     */
    public function getLastRecommendation()
    {
        return Recommendation::find()
            ->joinWith(['order'])
            ->andWhere(['order.apparatus_client_id' => $this->apparatus_client_id])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    public function getPrevOrder()
    {
        //Получаем предыдущую заявку для аппарата
        return Order::find()
            ->andWhere(['apparatus_client_id' => $this->apparatus_client_id])
            ->andWhere(['<', 'id', $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * Рекомендации из предыдущей заявки
     * @return string
     */
    public function getSLRecommendation()
    {
        $prev_order = $this->getPrevOrder();

        if (!$prev_order) return '';

        Yii::info('Previous Order id: ' . $prev_order->id, 'test');

        $str = [];
        //Перебираем необходимые работы из прошлой заявки
        /** @var Work $work */
        foreach ($this->getNeededWorks($prev_order->id) as $work) {
            $str[] = $work->content;
        }

        //Перебираем необходимые ЗИП из прошлой заявки
        /** @var Spta $spta */
        foreach ($this->getNeededSpta($prev_order->id) as $spta) {
            $str[] = $spta->name . ' (' . $spta->num . 'шт.)';
        }

        return implode('<br>', $str);
    }

    /**
     * Возвращает последнюю работу для аппарата
     * @return Work
     */
    public function getLastWork()
    {
        return Work::find()
            ->joinWith(['order'])
            ->andWhere(['order.apparatus_client_id' => $this->apparatus_client_id])
            ->orderBy(['id' => SORT_DESC])
            ->one();
    }

    /**
     * Возвращает последние работы для аппарата если статус работы "Необходимые"
     * @param int $id Идентификатор заявки
     * @return Work[]
     */
    public function getNeededWorks($id)
    {
        return Work::find()
            ->andWhere(['status' => Work::STATUS_NEED])
            ->andWhere(['order_id' => $id])
            ->all();
    }

    /**
     * Возвращает последние ЗиП для аппарата если статус ЗиП "Необходимые"
     * @param int $id Идентификатор заявки
     * @return Spta[]
     */
    public function getNeededSpta($id)
    {
        return Spta::find()
            ->andWhere(['type' => Spta::TYPE_NEED])
            ->andWhere(['order_id' => $id])
            ->all();
    }

    /**
     * Изет заявку согласно условиям:
     * Заявка на ремонт аппарата является повторной, если прошло менее 2ух дней после закрытия предыдущей заявки на этот же аппарат
     * Исключение работы данного правила, если предыдущая заявка имела тип работ «Подготовка»
     * @return bool
     */
    public function testOfRepeat()
    {
        if (!$this->apparatus_client_id) {
            return false;
        }

        $work_type_prepare = self::WORK_TYPE_PREPARE;
        $bad_status = self::STATUS_CANCEL;

        $sql = <<<SQL
          SELECT * FROM `order` 
          WHERE (`apparatus_client_id` = $this->apparatus_client_id) 
          AND (`done_at` <= CURDATE()) 
          AND (`done_at` >= DATE_SUB(CURDATE(),Interval 2 DAY)) 
          AND (`status` <> $bad_status)
          AND (`work_type` <> $work_type_prepare)
SQL;

        return Order::findBySql($sql)->exists();
    }

    /**
     * Получает для предыдущих 20ти заявк все ЗиП и работы для аппарата, сортировка по дате закрытия заявки. Новые сверху.
     * @return array
     *[
     *  'order' => [
     *      ID_заявки => [
     *          'done_at' => '2020-06-05 00:00:00',
     *          'c1' => 111,
     *          'c2' => 222,
     *          'c3' => 333,
     *          'engineer' => 'Петрович',
     *          'data' => [
     *              [
     *                  'description' => 'Штука 1',
     *                  'num' => 2.0,
     *              ],
     *              [
     *                  'description' => 'Штука 2',
     *                  'num' => 3.0,
     *              ],
     *              [
     *                  'description' => 'Нужно понажимать',
     *                  'num' => '-',
     *              ],
     *              [
     *                  'description' => 'Нужно потрогать',
     *                  'num' => '-',
     *              ],
     *          ],
     *      ],
     *      22 => [
     *          'done_at' => null,
     *          'c1' => 333,
     *          'c2' => 444,
     *          'c3' => 555,
     *          'engineer' => 'Наташка',
     *          'data' => null,
     *      ],
     *  ]
     *]
     */
    public function getSptaAndWorksForPrevOrder()
    {
        //Получаем 20 последних заявок
        $orders = self::find()
            ->andWhere(['apparatus_client_id' => $this->apparatus_client_id])
            ->andWhere(['<', 'id', $this->id])
            ->andWhere(['status' => self::STATUS_DONE])
            ->limit(20)
            ->orderBy(['done_at' => SORT_DESC])
            ->all();

        //Формируем список для оборотной страницы СЛ

        $list = [];

        foreach ($orders as $order) {
            $list['order'][$order->id] = [];
            $order_info =  &$list['order'][$order->id];
            $order_info['done_at'] = $order->done_at;
            $order_info['c1'] = $order->counter_1;
            $order_info['c2'] = $order->counter_2;
            $order_info['c3'] = $order->counter_3;
            $order_info['engineer'] = $order->engineer ? $order->engineer->fio : '';

            $data = &$order_info['data'];
            $data = [];
            //Перибираем запчасти
            foreach ($order->sptas as $spta) {
                //Только установленные
                if ($spta->type == $spta::TYPE_NEED) continue;
                if ($spta->name) {
                    $data[] = ['description' => $spta->name, 'num' => $spta->num];
                }
            }

            //Перибираем работы
            foreach ($order->works as $work) {
                //Только выполненные
                if ($work->status == $work::STATUS_NEED) continue;
                if ($work->content) {
                    $data[] = ['description' => $work->content, 'num' => '-'];
                }
            }
        }

        Yii::info($list, 'test');

        return $list;

    }

}
