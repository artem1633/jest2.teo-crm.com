<?php

namespace app\models;

use app\models\query\ApparatusToClientQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "apparatus_to_client".
 *
 * @property int $id
 * @property int $apparatus_id Аппарат
 * @property int $client_id Клиент
 * @property string $notes Примечание
 * @property int $is_owner Является ли клиент собственником аппарата
 * @property string $serial_number Серийный номер аппарата
 * @property string $brand Марка аппарата
 * @property mixed $apparatus_info Марка модель серийный номер
 * @property string $owner Владелец переводимого аппарата (тот кто передает аппарат)
 * @property int $new_owner_id Получатель переводимого аппарата (тот кому отдают аппарат)
 *
 * @property Apparatus $apparatus
 * @property Client $client
 * @property Report[] $reports
 * @property Spta[] $sptas //ЗиП для аппарата клиента
 * @property Work[] $works //Работы для аппарата клиента
 * @property Order[] $orders //Заявки для аппарата клиента
 * @property User[] $engineers
 */
class ApparatusToClient extends ActiveRecord
{
    public $brand;
    public $apparatus_info;
    public $owner;
    public $new_owner_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apparatus_to_client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apparatus_id', 'client_id', 'is_owner'], 'integer'],
            [['notes'], 'string'],
            [['serial_number'], 'string', 'max' => 255],
            [
                ['apparatus_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Apparatus::className(),
                'targetAttribute' => ['apparatus_id' => 'id']
            ],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            [['new_owner_id'], 'integer'],
            [['owner'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'apparatus_id' => 'Аппарат',
            'client_id' => 'Клиент',
            'notes' => 'Примечание',
            'is_owner' => 'Собственник',
            'serial_number' => 'Серийный номер',
            'owner' => 'Клиент',
            'new_owner_id' => 'Клиент - получатель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::className(), ['id' => 'apparatus_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::class, ['apparatus_client_id' => 'id']);
    }

    /**
     * Все ЗиП для аппарата клиента
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getSptas()
    {
        return $this->hasMany(Spta::class, ['order_id' => 'id'])
            ->viaTable(Order::tableName(), ['id' => 'apparatus_client_id']);
    }

    /**
     * Все работы для аппарата клиента
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getWorks()
    {
        return $this->hasMany(Work::class, ['order_id' => 'id'])
            ->viaTable(Order::tableName(), ['id' => 'apparatus_client_id']);
    }

    /**
     * Все инженеры, работавшие с аппаратом слиента
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getEngineers()
    {
        return $this->hasMany(User::class, ['id' => 'engineer_id'])
            ->viaTable(Order::tableName(), ['apparatus_client_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ApparatusToClientQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApparatusToClientQuery(get_called_class());
    }

    /**
     * Получает список аппартов для клиента (Марка модель серйный номер)
     * @param integer $id Идентификатор клиента
     * @return array
     */
    public function getApparatusListForClient($id = null)
    {
        if (!$id) return [];
//        if (!$id){
//            /** @var Users $identity */
//            $identity = Yii::$app->user->identity;
//            $client_id = $identity-> client_id;
//        } else {
        $client_id = $id;
//        }
//

        $sql = <<<SQL
        SELECT at.id, CONCAT(b.name, ' ', a.model, ' №', at.serial_number) as apparatus_info FROM apparatus_to_client at
        LEFT JOIN apparatus a ON at.apparatus_id = a.id
        LEFT JOIN brand b ON a.brand_id = b.id
        WHERE at.client_id = $client_id
        ORDER BY (b.name)
SQL;


        return ArrayHelper::map(ApparatusToClient::findBySql($sql)->all(), 'id', 'apparatus_info');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::class, ['apparatus_to_client_id' => 'id']);
    }

    /**
     * Получает все запчасти для аппарата
     * @param int $type Тип (Установленные/Необходимые)
     * @return Spta[]|array
     */
    public function getAllSpta($type = null)
    {
        $orders_id = Order::find()
            ->andWhere(['apparatus_client_id' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->select('id')
            ->limit(20)
            ->column();

        $query = Spta::find()
            ->andWhere(['IN', 'order_id', $orders_id]);

        if ($type) {
            $query->andWhere(['type' => $type]);
        }

        return $query->all();
    }

    /**
     * Получает последние работы для аппарата из 20 последних заявок
     * @param int $status Статус (Выполненные/Необходимые)
     * @return Spta[]|array
     */
    public function getAllWorks($status = null)
    {
        $orders_id = Order::find()
            ->andWhere(['apparatus_client_id' => $this->id])
            ->orderBy(['id' => SORT_DESC])
            ->select('id')
            ->limit(20)
            ->column();


        $query = Work::find()
            ->andWhere(['IN', 'order_id', $orders_id]);

        if ($status) {
            $query->andWhere(['status' => $status]);
        }

        return $query->all();
    }

    /**
     * Получает список всех серийников
     */
    public function getSNList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'serial_number');
    }

    /**
     * Получает все ЗиП и работы для аппарата, сортировка по дате закрытия заявки. Новые сверху.
     * @return array
     *[
     *  'order' => [
     *      ID_заявки => [
     *          'done_at' => '2020-06-05 00:00:00',
     *          'c1' => 111,
     *          'c2' => 222,
     *          'c3' => 333,
     *          'engineer' => 'Петрович',
     *          'data' => [
     *              [
     *                  'description' => 'Штука 1',
     *                  'num' => 2.0,
     *              ],
     *              [
     *                  'description' => 'Штука 2',
     *                  'num' => 3.0,
     *              ],
     *              [
     *                  'description' => 'Нужно понажимать',
     *                  'num' => '-',
     *              ],
     *              [
     *                  'description' => 'Нужно потрогать',
     *                  'num' => '-',
     *              ],
     *          ],
     *      ],
     *      22 => [
     *          'done_at' => null,
     *          'c1' => 333,
     *          'c2' => 444,
     *          'c3' => 555,
     *          'engineer' => 'Наташка',
     *          'data' => null,
     *      ],
     *  ]
     *]
     */
    public function getSptaAndWorks()
    {
        //Получаем 20 последних заявок
        $orders = Order::find()
            ->andWhere(['apparatus_client_id' => $this->id])
            ->andWhere(['status' => Order::STATUS_DONE])
            ->orderBy(['done_at' => SORT_DESC])
            ->all();

        //Формируем список для оборотной страницы СЛ
        $list = [];

        /** @var Order $order */
        foreach ($orders as $order) {
            $list['order'][$order->id] = [];
            $order_info =  &$list['order'][$order->id];
            $order_info['done_at'] = $order->done_at;
            $order_info['c1'] = $order->counter_1;
            $order_info['c2'] = $order->counter_2;
            $order_info['c3'] = $order->counter_3;
            $order_info['engineer'] = $order->engineer ? $order->engineer->fio : '';

            $data = &$order_info['data'];
            $data = [];
            //Перибираем запчасти
            foreach ($order->sptas as $spta) {
                //Только установленные
                if ($spta->type == $spta::TYPE_NEED) continue;
                if ($spta->name) {
                    $data[] = ['description' => $spta->name, 'num' => $spta->num];
                }
            }

            //Перибираем работы
            foreach ($order->works as $work) {
                //Только выполненные
                if ($work->status == $work::STATUS_NEED) continue;
                if ($work->content) {
                    $data[] = ['description' => $work->content, 'num' => '-'];
                }
            }
        }

        Yii::info($list, 'test');

        return $list;

    }
}
