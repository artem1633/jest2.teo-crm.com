<?php

namespace app\models;


/**
 * This is the model class for table "client_to_notify".
 *
 * @property int $id
 * @property int $client_id
 * @property string $notify_date Дата отправки напоминания о необходимости отчета
 *
 * @property Client $client
 */
class ClientToNotify extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client_to_notify';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['notify_date'], 'safe'],
            [['client_id'], 'exist', 'skipOnError' => true, 'targetClass' => Client::className(), 'targetAttribute' => ['client_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'notify_date' => 'Дата отправки напоминания о необходимости отчета',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ClientToNotifyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ClientToNotifyQuery(get_called_class());
    }
}
