<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "complaint".
 *
 * @property int $id
 * @property int $order_id Заявка
 * @property string $header Заголовок
 * @property string $text Текст
 * @property string $created_at
 *
 * @property Order $order
 */
class Complaint extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'complaint';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['header'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заявка',
            'header' => 'Заголовок',
            'text' => 'Текст',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!$this->created_at){
            $this->created_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ComplaintQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ComplaintQuery(get_called_class());
    }

    /**
     * Возвращает кол-во жалоб для заявки
     * @param int $id Идентификатор заявки
     * @return int|string
     */
    public function getCount($id)
    {
        return self::find()->forOrder($id)->count();
    }
}
