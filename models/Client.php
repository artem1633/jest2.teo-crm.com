<?php

namespace app\models;

use app\models\constants\UsersConstants;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "client".
 *
 * @property int $id
 * @property string $official_name Наименование
 * @property string $address Адрес
 * @property string $contact_person Контактное лицо
 * @property int $phone_number Телефон
 * @property string $email Email
 * @property int $countUsers Кол-во пользователей для клиента
 * @property int $contract_path Путь к файлу контракта
 *
 * @property ApparatusToClient[] $apparatusToClients
 * @property Contract $contract
 * @property Order[] $orders
 * @property Repairing[] $repairings
 * @property Users[] $users
 * @property ApparatusToClient[] $apparatusToClient Аппараты клиента
 * @property int $ordersCount Кол-во заказов клиента
 */
class Client extends ActiveRecord
{
    public $apparatus_info;
    public $contract_path;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'client';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['address', 'contract_path'], 'string'],
            [['phone_number'], 'integer'],
            [['official_name', 'contact_person', 'email'], 'string', 'max' => 255],
            [['email'], 'email'],
            [['email', 'official_name'], 'unique'],
            [['official_name'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'official_name' => 'Наименование',
            'address' => 'Адрес',
            'contact_person' => 'Контактное лицо',
            'phone_number' => 'Телефон',
            'email' => 'Email',
            'apparatus' => 'Аппараты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClients()
    {
        return $this->hasMany(ApparatusToClient::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContract()
    {
        return $this->hasOne(Contract::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairings()
    {
        return $this->hasMany(Repairing::className(), ['client_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(Users::className(), ['client_id' => 'id']);
    }

    /**
     * Загрузка файла с реквизитами
     */
    public function uploadBankDetails()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_DOCUMENT;

        $form->file = UploadedFile::getInstance($this, 'bank_details_file');

        if (!$form->file) {
            return false;
        }

        $tmp_name = 'contract';
        $target_dir = 'uploads/client/' . $this->id;

        if ($file_path = $form->upload($target_dir, $tmp_name, true)) {
            //Файл успешно загружен
            $this->bank_details_file = $file_path;
            if (!$this->save()) {
                Yii::error($this->errors, '_error');
            } else {
                return true;
            }
        } else {
            Yii::error($form->errors, '_error');
        }
        return false;
    }

    /**
     * Получает кол-во учетных записей для клиента
     * @return int
     */
    public function getCountUsers()
    {
        return Users::find()->andWhere(['client_id' => $this->id])->count();
    }

    /**
     * Создает пользователя системы для клиента
     * @return Users|bool
    //     * @throws \yii\base\Exception
     */
//    public function createUser()
//    {
//        $password = rand(100000, 9999999);
//        $user = new Users([
//            'fio' => 'Пользователь ' . ($this->getCountUsers() + 1) . '. ' . $this->official_name,
//            'role' => UsersConstants::ROLE_CLIENT,
//            'login' => explode('@', $this->email)[0] . '_' . rand(100000, 9999999),
//            'password' => $password,
//            'client_id' => $this->id,
//        ]);
//
//        if (!$user->save()) {
//            Yii::error($user->errors, '_errors');
//            return false;
//        }
//        $user->open_password = $password;
//        return $user;
//    }

    /**
     * Получает аппараты для клиента
     */
    public function getApparatusList()
    {
        $sql = <<<SQL
            SELECT CONCAT(b.name, ' ', a.model, ' (', ac.serial_number, ')' ) AS apparatus_info  FROM apparatus_to_client ac
            LEFT JOIN apparatus a ON ac.apparatus_id = a.id
            LEFT JOIN brand b ON a.brand_id = b.id
SQL;
        $apparatus = [];
        /** @var ApparatusToClient $item */
        foreach (ApparatusToClient::findBySql($sql)->each() as $item) {
//            Yii::info($item->attributes, 'test');
            array_push($apparatus, $item->apparatus_info);
        }

        Yii::info($apparatus, 'test');

        return '<p style="font-size: 1rem;">' . implode('<br>', $apparatus) . '</p>';
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'official_name');
    }

    public function getOrdersCount()
    {
        return Order::find()
            ->andWhere(['client_id' => $this->id])
            ->count();
    }
}
