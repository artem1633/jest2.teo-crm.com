<?php

namespace app\models;

use app\models\constants\UsersConstants;
use app\models\query\UsersQuery;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $fio ФИО
 * @property int $role Роль
 * @property string $login Логин
 * @property string $password Пароль
 * @property string $password_reset_token
 * @property string $new_password Новый пароль
 * @property string $created_at
 * @property int $client_id Клиент
 * @property string $avatar Путь к аватару
 * @property UploadedFile $image Загружаемый файл
 * @property string $open_password Пароль показываемый админу при создании пользователя для клиента
 *
 * @property Client $client
 */
class Users extends ActiveRecord
{
    public $new_password;
    public $image;
    public $open_password;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role', 'client_id'], 'integer'],
            [['created_at', 'image', 'password', 'new_password', 'open_password'], 'safe'],
            [['fio', 'login', 'password_reset_token', 'avatar'], 'string', 'max' => 255],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fio' => 'ФИО',
            'role' => 'Роль',
            'login' => 'Логин',
            'password' => 'Пароль',
            'new_password' => 'Новый пароль',
            'password_reset_token' => 'Password Reset Token',
            'created_at' => 'Создан',
            'client_id' => 'Клиент',
            'avatar' => 'Аватар',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\base\Exception
     */
    public function beforeSave($insert)
    {
        if ($this->isNewRecord && $this->password) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }

        if ($this->new_password != null) {
            $this->password = Yii::$app->security->generatePasswordHash($this->new_password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * {@inheritdoc}
     * @return UsersQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }

    public function getTypeDescription()
    {
        return UsersConstants::getRoleLabel($this->role);
    }

    public function getRoles()
    {
        return UsersConstants::getArray();
    }

    public function getAvatar()
    {
        return UsersConstants::getImage($this->avatar);
    }

    /**
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @return bool
     */
    public function uploadAvatar()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_IMAGE;
        $form->file = UploadedFile::getInstance($this, 'image');
//        Yii::info($_FILES, 'test');
//        Yii::info($form->file, 'test');

        if ($form->file) {
            $fileName = $form->upload('uploads/avatars', Yii::$app->security->generateRandomString());
            Yii::$app->db
                ->createCommand()
                ->update('users', ['avatar' => $fileName], ['id' => $this->id])
                ->execute();
        }

        return true;
    }

    /**
     * Сохраняет в файл инфу о пользователе
     * @return string Путь к файлу
     */
    public function saveInfo()
    {
        $info = [
            'Клиент: ' . $this->client->official_name ?? '',
            'ФИО: ' . $this->fio,
            'Логин: ' . $this->login,
            'Пароль: ' . $this->open_password,
        ];

        $dir = 'uploads/temp';
        if (!is_dir($dir)) {
            mkdir($dir, 0777, true);
        } else {
            $this->clearDir($dir);
        }

        $file_path = $dir . '/' . $this->id . 'userInformation.txt';

        $file = fopen($file_path, "a+");
        $text = implode(PHP_EOL, $info);
        flock($file, LOCK_EX);
        fwrite($file, $text);
        flock($file, LOCK_UN);
        fclose($file);

        return $file_path;
    }

    /**
     * Удаление всех файлов из папки
     * @param string $path_dir Путь к очищаемой папке
     * @return bool
     */
    private function clearDir($path_dir)
    {
        $files = array_diff(scandir($path_dir), ['..', '.', '.gitignore']);
        foreach ($files as $file_name) {
            $file = $path_dir . '/' . $file_name;
            if (is_file($file)) {
                try {
                    unlink($file);
                } catch (\Exception $e) {
                    Yii::error($e->getMessage(), '_error');
                }
            }
        }
        return true;
    }

    /**
     * Возвращает массив инденеров
     * @return array
     */
    public function getEngineerList()
    {
        return ArrayHelper::map(self::find()->andWhere(['role' => UsersConstants::ROLE_ENGINEER])->all(), 'id', 'fio');
    }
}
