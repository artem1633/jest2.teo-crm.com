<?php

namespace app\models\query;

use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\app\models\Complaint]].
 *
 * @see \app\models\Complaint
 */
class ComplaintQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forOrder($id)
    {
        return $this->andWhere(['order_id' => $id]);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Complaint[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Complaint|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
