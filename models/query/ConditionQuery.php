<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Condition]].
 *
 * @see \app\models\Condition
 */
class ConditionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    public function forOrder($id)
   {
       return $this->andWhere(['order_id' => $id]);
   }

    /**
     * {@inheritdoc}
     * @return \app\models\Condition[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Condition|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
