<?php

namespace app\models\query;

/**
 * This is the ActiveQuery class for [[\app\models\Recommendation]].
 *
 * @see \app\models\Recommendation
 */
class RecommendationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\Recommendation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\Recommendation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
