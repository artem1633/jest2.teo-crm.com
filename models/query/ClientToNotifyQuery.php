<?php

namespace app\models\query;

use yii\db\ActiveQuery;


/**
 * This is the ActiveQuery class for [[\app\models\ClientToNotify]].
 *
 * @see \app\models\ClientToNotify
 */
class ClientToNotifyQuery extends ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \app\models\ClientToNotify[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\ClientToNotify|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }


    /**
     * Добавляет усовие с дипазаоном дат от начала до конца указанного или текущего месяца
     * @param int $num_month Порядковый номер месяца
     * @return $this
     */
    public function forMonth($num_month = null)
    {
        if (!$num_month){
            $num_month = date('n', time());
        }

        $start_date = date('Y-' . $num_month . '-01 00:00:00', time());
        $end_date = date('Y-' . $num_month . '-t 23:59:59', time());

        return $this->andWhere(['BETWEEN', 'notify_date', $start_date, $end_date]);
    }

    /**
     * Выборка для клиента
     * @param int $id Идентификатор клиента
     * @return $this
     */
    public function forClient($id)
    {
        return $this->andWhere(['client_id' => $id]);
    }

    /**
     * Выборка для одного дня, без указания дня - для текущей даты
     * @param string $date Целевая дата (формат любой, понятный для функции strtotime)
     * @return $this
     */
    public function forDay($date = null)
    {
        if (!$date){
            $date = date('Y-m-d H:i:s', time());
        }

        $start_date = date('Y-m-d 00:00:00', strtotime($date));
        $end_date = date('Y-m-d 23:59:59', strtotime($date));

        return $this->andWhere(['BETWEEN', 'notify_date', $start_date, $end_date]);
    }
}
