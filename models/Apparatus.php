<?php

namespace app\models;

use app\models\query\ApparatusQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "apparatus".
 *
 * @property int $id
 * @property int $brand_id Марка
 * @property string $model Модель
 * @property int $type Тип принтера (1-Монохром, 2-Цветной)
 * @property string $brand_string Марка, вводящаяся вручную
 * @property array $list Массив аппаратов
 * @property string $apparatus_name Марка + модель аппарата
 *
 * @property Brand $brand
 * @property ApparatusToClient[] $apparatusToClients
 * @property Order[] $orders
 * @property Repairing[] $repairings
 * @property array $typeList
 */
class Apparatus extends ActiveRecord
{
    const TYPE_MONOCHROME = 1;
    const TYPE_COLORED = 2;

    public $brand_string;

    public $apparatus_name;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apparatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['brand_id', 'type'], 'integer'],
            [['model', 'brand_string', 'apparatus_name'], 'string', 'max' => 255],
            [
                ['brand_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Brand::className(),
                'targetAttribute' => ['brand_id' => 'id']
            ],
            [
                ['brand_id', 'brand_string'],
                'required',
                'message' => 'Необходимо выбрать/ввести марку',
                'when' => function (Apparatus $model) {
                    if ($model->brand_id || $model->brand_string) {
                        //Если марка выбрана или введена вручную
                        return false;
                    } else {
                        return true;
                    }
                }
            ],
            [['model'], 'required'],
            [
                ['type'],
                'required',
                'message' => 'Необходимо выбрать тип аппарата',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'brand_id' => 'Марка',
            'brand_string' => 'Марка',
            'model' => 'Модель',
            'type' => 'Тип',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClients()
    {
        return $this->hasMany(ApparatusToClient::className(), ['apparatus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['apparatus_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRepairings()
    {
        return $this->hasMany(Repairing::className(), ['apparatus_id' => 'id']);
    }

    /**
     * {@inheritdoc}
     * @return ApparatusQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ApparatusQuery(get_called_class());
    }

    /**
     * Массив типов аппарата для выпадающего списка
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_MONOCHROME => 'Монохромный',
            self::TYPE_COLORED => 'Цветной',
        ];
    }

    /**
     * Получает наименование типа аппарата
     * @param int $type Тип аппарата 1 - монохром, 2 - цветной
     * @return mixed
     */
    public function getTypeName($type)
    {
        return $this->getTypeList()[$type];
    }

    /**
     * Если марка указана вручную - добавляет марку в базу
     * @return bool
     */
    public function addBrand()
    {
        if ($this->brand_string) {
            $brand = new Brand(['name' => $this->brand_string]);
            if (!$brand->save()) {
                \Yii::error($brand->errors, '_error');
                return false;
            }
            $this->brand_id = $brand->id;
        }
        return true;
    }

    /**
     * Получает список аппаратов каждая строка в формате "Марка  модель"
     * @return array
     */
    public function getList()
    {
        $sql = <<<SQL
        SELECT apparatus.id, CONCAT(brand.name, ' ' , apparatus.model) as apparatus_name FROM apparatus 
        LEFT JOIN brand ON brand.id = apparatus.brand_id
SQL;

        return ArrayHelper::map( Apparatus::findBySql($sql)->all(), 'id', 'apparatus_name');
    }

    /**
     * Возвращает наименования марки и модели для аппарата
     * @return string
     */
    public function getApparatusFullName()
    {
        \Yii::info($this->attributes, 'test');

        return $this->brand->name . ' ' . $this->model;

    }

    /**
     * Возвращает тип аппарата (монохром/цветной)
     * @param integer $id Идентификатор аппарата
     * @return int
     */
    public static function getType($id)
    {
        if (!$id) return 0;
        return self::findOne($id)->type;
    }
}
