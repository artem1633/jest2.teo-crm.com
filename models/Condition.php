<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "condition".
 *
 * @property int $id
 * @property int $order_id Заявка
 * @property string $created_at
 * @property string $description Описание
 *
 * @property Order $order
 */
class Condition extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'condition';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['created_at'], 'safe'],
            [['description'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заявка',
            'created_at' => 'Дата создания',
            'description' => 'Состояние аппарата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\ConditionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\ConditionQuery(get_called_class());
    }
}
