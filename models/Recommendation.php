<?php

namespace app\models;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "recommendation".
 *
 * @property int $id
 * @property int $order_id Заявка
 * @property string $header Заголовок
 * @property string $text Текст
 * @property string $created_at
 *
 * @property Order $order
 */
class Recommendation extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'recommendation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
            [['header'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заявка',
            'header' => 'Заголовок',
            'text' => 'Текст',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\RecommendationQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\RecommendationQuery(get_called_class());
    }
}
