<?php

namespace app\models\constants;

use app\models\Users;
use Yii;
use yii\helpers\Url;


class UsersConstants extends Users
{
    const ROLE_SUPER_ADMIN = 1;
    const ROLE_MANAGER = 2;
    const ROLE_ENGINEER = 3;

    public static function getRoleLabel($role)
    {
        switch ($role) {
            case self::ROLE_SUPER_ADMIN:
                return 'Супер Администратор';
            case self::ROLE_MANAGER:
                return 'Диспетчер';
                case self::ROLE_ENGINEER:
                return 'Инженер';
        }
        return 'Неизвестный';
    }

    public static function getArray()
    {
        if (self::isSuperAdmin()){
            return [
                self::ROLE_SUPER_ADMIN => self::getRoleLabel(self::ROLE_SUPER_ADMIN),
                self::ROLE_MANAGER => self::getRoleLabel(self::ROLE_MANAGER),
                self::ROLE_ENGINEER => self::getRoleLabel(self::ROLE_ENGINEER),
            ];
        } else {
            return [
                self::ROLE_MANAGER => self::getRoleLabel(self::ROLE_MANAGER),
                self::ROLE_ENGINEER => self::getRoleLabel(self::ROLE_ENGINEER),
            ];
        }

    }

    public static function getImage($image)
    {
        if (!file_exists($image) || $image == '') {
            $path = Url::to(['@web/img/nouser.png']);
        } else {
            $path = Url::to(['@web/' . $image]);
        }
        return $path;
    }

    /**
     * Проверяет пользователя на админа
     * @return bool
     */
    public static function isSuperAdmin()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        if ($identity) {
            return $identity->role === self::ROLE_SUPER_ADMIN;
        }

        return false;
    }

    /**
     * Проверяет пользователя на админа
     * @return bool
     */
    public static function isAdministration()
    {
        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        if ($identity) {
            return ($identity->role === self::ROLE_SUPER_ADMIN || $identity->role === self::ROLE_MANAGER);
        }

        return false;
    }


}
