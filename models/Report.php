<?php

namespace app\models;

use app\models\constants\UsersConstants;
use app\models\query\ReportQuery;
use app\components\EmailHelper;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\UploadedFile;

/**
 * This is the model class for table "report".
 *
 * @property int $id
 * @property string $created_at
 * @property string $sented_at Дата отправки
 * @property int $apparatus_to_client_id
 * @property int $counter_1 Показания счетчика 1
 * @property int $counter_2 Показания счетчика 2
 * @property int $counter_3 покказания счетчика 3
 * @property string $photo Фото счетчика
 * @property int $report_month Отчетный месяц
 * @property int $report_year Отчетный год
 * @property int $status Статус отчета
 * @property int $apparatus_id Идентификатор аппарата
 * @property string $client_id Идентификатор клиента
 * @property int $client_name Наименование клиента
 * @property int $apparatus_type Тип аппарата
 * @property int $apparatus_full_name Марка, модель, серийник
 *
 * @property ApparatusToClient $apparatusToClient
 * @property Apparatus $apparatus
 * @property Client $client
 *
 */
class Report extends ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_NEW = 2;
    const STATUS_APPROVED = 3;
    const STATUS_REJECT = 4;

    const SCENARIO_MONOCHROME = 'monochrome';
    const SCENARIO_COLORED = 'colored';

    public $apparatus_id;
    public $apparatus_type;
    public $apparatus_full_name;
    public $client_id;
    public $client_name;
    public $counter_3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null,
                'createdAtAttribute' => 'created_at',
                'value' => date('Y-m-d H:i:s', time()),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['created_at', 'sented_at'], 'safe'],
            [
                [
                    'apparatus_to_client_id',
                    'counter_1',
                    'counter_2',
                    'counter_3',
                    'status',
                    'apparatus_id',
                    'client_id',
                    'report_year',
                    'report_month',
                    'apparatus_type',
                ],
                'integer'
            ],
            [['photo'], 'string'],
            [['apparatus_id', 'client_id', 'report_month', 'report_year'], 'required'],
            [['counter_1'], 'required', 'on' => self::SCENARIO_MONOCHROME],
            [['counter_2', 'counter_3'], 'required', 'on' => self::SCENARIO_COLORED],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'sented_at' => 'Дата отправки',
            'apparatus_to_client' => 'Apparatus To Client',
            'counter_1' => 'Показания счетчика 1',
            'counter_2' => 'Счетчик отпечатков (цветной)',
            'photo' => 'Фото счетчика/ов',
            'report_month' => 'Отчетная дата',
            'status' => 'Статус',
            'apparatus_id' => 'Аппарат',
//            'client_id' => 'Клиент',
            'report_year' => 'Отчетный год',
            'client_name' => 'Клиент',
            'apparatus_full_name' => 'Аппарат',
        ];
    }

    /**
     * {@inheritdoc}
     * @return ReportQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ReportQuery(get_called_class());
    }

    /**
     * Список месяцев
     * @return array
     */
    public function getMonthList()
    {
        return [
            1 => 'Январь',
            2 => 'Февраль',
            3 => 'Март',
            4 => 'Апрель',
            5 => 'Май',
            6 => 'Июнь',
            7 => 'Июль',
            8 => 'Август',
            9 => 'Сентябрь',
            10 => 'Октябрь',
            11 => 'Ноябрь',
            12 => 'Декабрь'
        ];
    }

    /**
     * Список годов (предыдущий, текущий и следующий)
     * @return array
     */
    public function getYearList()
    {
        $current_year = date('Y', time());
        return [
            $current_year - 1 => $current_year - 1,
            $current_year => $current_year,
            $current_year + 1 => $current_year + 1,
        ];
    }

    public function getStatusesList()
    {
        return [
            self::STATUS_DRAFT => 'Черновик',
            self::STATUS_NEW => 'Новый',
            self::STATUS_APPROVED => 'Подтверждён',
            self::STATUS_REJECT => 'Отклонен',
        ];
    }

    /**
     * Возвращает наименование статуса
     * @param int $status Статус
     * @return string
     */
    public function getStatusName($status)
    {
        return $this->getStatusesList()[$status];
    }

    /**
     * @return bool
     * @throws \yii\db\Exception
     */
    public function uploadFile()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_IMAGE;

        $target_dir = 'uploads/counters/report/' . $this->client_id . '/' . $this->id;

        Yii::info('Целевая папка: ' . $target_dir, 'test');
        $form->file = UploadedFile::getInstance($this, 'photo');
        Yii::info($form->file, 'test');

        if (!$form->file) {
            return false;
        }

        $target_name = 'photo';
        $path_file = '';

        if ($form->file) {
            Yii::info('Имя файла: ' . $target_name, 'test');
            $path_file = $form->upload($target_dir, $target_name, true);
        } else {
            Yii::info('Файл не загружен', 'test');
        }

        Yii::$app->db
            ->createCommand()
            ->update('report', ['photo' => $path_file], ['id' => $this->id])
            ->execute();
        return true;
    }

    public function checkFile()
    {
        $form = new UploadForm();
        $form->file = UploadedFile::getInstance($this, 'photo');

        if (!$form->file) {
            return false;
        }
        return true;
    }

    /**
     * Подготовка данных рперед сохранением
     * @return bool
     */
    public function prepareSave()
    {
        Yii::info($this->apparatus_type, 'test');

        if ($this->apparatus_type == 1) {
            $this->scenario = self::SCENARIO_MONOCHROME;
        } else {
            $this->scenario = self::SCENARIO_COLORED;
            $this->counter_1 = $this->counter_3;
        }

        $this->apparatus_to_client_id = ApparatusToClient::findOne([
                'apparatus_id' => $this->apparatus_id,
                'client_id' => $this->client_id,
            ])->id ?? null;
        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatusToClient()
    {
        return $this->hasOne(ApparatusToClient::class, ['id' => 'apparatus_to_client_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatus()
    {
        return $this->hasOne(Apparatus::class, ['id' => 'apparatus_id'])->via('apparatusToClient');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::class, ['id' => 'client_id'])
            ->via('apparatusToClient');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::class, ['id' => 'brand_id'])
            ->via('apparatus');
    }

    /**
     * @return string
     */
    public function getCountersPhoto()
    {
        $photo_path = $this->photo;
        Yii::info(pathinfo($this->photo), 'test');
        $photo_name = pathinfo($this->photo)['filename'];

        return <<<HTML
    <a data-lightbox="{$photo_name}" href="/{$photo_path}">
        <img style="max-width: 100px" src="/{$photo_path}" alt="{$photo_name}">
    </a>
HTML;
    }

    /**
     * Возвращает кнопку для смены статуса заявки на РМ
     * @return null|string
     */
    public function getButton()
    {
        $status = $this->status + 1;

        if ($status > self::STATUS_REJECT) {
            return null;
        }

        if ($status == self::STATUS_NEW) {
            return Html::a('Отправить', ['/report/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Отправить отчет',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Отправить отчет?',
                'data-confirm-message' => 'Подтвердите отправку показаний счетчика/ов',
                'data-confirm-ok' => 'Отправить',
                'data-confirm-cancel' => 'Отмена',
            ]);
        }
        if ($status == self::STATUS_NEW && UsersConstants::isSuperAdmin()) {
            return Html::a('Подтвердить', ['/repairing/change-status', 'id' => $this->id, 'status' => $status], [
                'class' => 'btn btn-info btn-sm',
                'title' => 'Подтвердить показания',
                'data-toggle' => 'tooltip',
                'role' => 'modal-remote',
                'data-confirm-title' => 'Подтвердить показания?',
                'data-confirm-message' => 'Подтвердите выбор',
                'data-confirm-ok' => 'Подтвердить ',
                'data-confirm-cancel' => 'Отмена',
            ]);
        }

        return null;
    }

    /**
     * Отправка почты админу о новом отчете
     * @return bool
     * @throws \HttpInvalidParamException
     */
    public function sendNotify()
    {
        $url = Url::to(['/report/view', 'id' => $this->id], true);

        $email = new EmailHelper();
        $email->email_type = EmailHelper::TYPE_REPORT;
        $email->subject = $this->client->official_name . ', '
            . $this->apparatus->getApparatusFullName() . ', '
            . $this->apparatusToClient->serial_number . ', показания счетчиков';

        $email->html_body = '<br><p>ссылка на раздел со счетчиками для сверки веденных данных с фотографиями: ' .
            Html::a($url, $url) . '</p>';

        $email->send();

        $this->sented_at = date('Y-m-d H:i:s', time());
        if ($this->save(false)) {
            Yii::error($this->errors, '_error');
        }
        return true;
    }

    /**
     * Получает наименование месяца
     * @param int $number номер месяца
     * @return string
     */
    public function getMonthName($number)
    {
        return $this->getMonthList()[$number];
    }

    /**
     * Определяет является ли клиент должником по отчетам.
     * @param int $client_id Идентификатор клиента
     * @param int $to_month порядковый номер месяца для которого проверятся задолженность
     * @return bool|array
     */
    public static function isDebtor($client_id = null, $to_month = null)
    {
        $identity = Yii::$app->user->identity;
        $current_day = date('j', time());

        if ($current_day < 21 && !$to_month) {
            //Если месяц текущий и 21 число еще не настало - пропускаем проверку должников
            return false;
        }

        if (!$to_month) {
            $to_month = date('n', time());
        }

        /** @var Users $identity */
        if (!$client_id) {
            $client = Client::findOne($identity->client_id) ?? null;
        } else {
            $client = Client::findOne($client_id) ?? null;
        }

        if (!$client) {
            return false;
        }

        //Находим аппараты для которых созданы отчеты в текущем/указанном месяце
        $to_month = str_pad($to_month, 2, '0', STR_PAD_LEFT);
        $start_date = date('Y-' . $to_month . '-01 00:00:00', time());
        $end_date = date('Y-' . $to_month . '-t 23:59:59', time());
        $apparatus_reported = ApparatusToClient::find()
            ->joinWith(['reports'])
            ->andWhere(['BETWEEN', 'report.created_at', $start_date, $end_date])
            ->select('apparatus_to_client.id')->column();

        Yii::info('Apparatus reported: ' . implode(', ', $apparatus_reported), 'test');

        //Получаем все аппараты
        $apparatus_all = ApparatusToClient::find()
            ->andWhere(['client_id' => $client->id])
            ->select('id')
            ->column();

        //Получаем id аппаратов (apparatus_to_client) без отчета
        $not_reported_apparatus = array_diff($apparatus_all, $apparatus_reported);

        Yii::info('Apparatus NOT reported: ' . implode(', ', $not_reported_apparatus), 'test');

        if ($not_reported_apparatus) {
            //Если есть аппараты для которых не создан очтет - значит должник
            return $not_reported_apparatus;
        }

        return false;
    }

    /**
     * Получает аппараты, по которым в текущем месяце не предоставлен отчет
     * @return array
     */
    public function getApparatusListForClient()
    {

        /** @var Users $identity */
        $identity = Yii::$app->user->identity;
        $client_id = $identity->client_id;

        $start_date = date('Y-m-1 00:00:00', time());
        $end_date = date('Y-m-t 23:59:59', time());

        //Аппараты клиента
        $app_ids = ApparatusToClient::find()
            ->forClient($client_id)
            ->select('id')
            ->column();

        //аппараты для которых есть отчеты в текущем месяце
        $app_ids_is_reported = Report::find()
            ->joinWith(['apparatusToClient'])
            ->select('report.apparatus_to_client_id')
            ->andWhere(['apparatus_to_client.client_id' => $client_id])
            ->andWhere(['BETWEEN', 'report.created_at', $start_date, $end_date])
            ->groupBy('report.apparatus_to_client_id')
            ->column();

        //аппараты для которых нет отчета в текущем месяце
        $debtor_apparatus_ids = array_diff($app_ids, $app_ids_is_reported);

        $debtor_apparatus_ids = implode(', ', $debtor_apparatus_ids);
        Yii::info('Задолженность по аппаратам: ' . $debtor_apparatus_ids, 'test');

        $sql = <<<SQL
        SELECT a.id, CONCAT(b.name, ' ', a.model, ' №', at.serial_number) as apparatus_info FROM apparatus_to_client at
        LEFT JOIN apparatus a ON at.apparatus_id = a.id
        LEFT JOIN brand b ON a.brand_id = b.id
        IN ($debtor_apparatus_ids) 
        ORDER BY (b.name)
SQL;
        //Не работает
        return ArrayHelper::map(ApparatusToClient::findBySql($sql)->all(), 'id', 'apparatus_info');
    }

    /**
     * Проверка отчетности, отправка писем должникам
     * @return bool
     * @throws \HttpInvalidParamException
     */
    public static function checkReports()
    {
        $report_day = (new Settings)->getValueByKey('report_day');
        if ($report_day >= date('j', time())) {
            return false;
        }


        /** @var Client $client */
        foreach (Client::find()->each() as $client) {
            if (ClientToNotify::find()->forClient($client->id)->forMonth()->count() >= 3 ||
            ClientToNotify::find()->forClient($client->id)->forDay()->count() > 0){
                //Если клиенту в текущем месяце отправлено уже 3 сообщения или сегодня уже отправлялось
                continue;
            }
                $app_ids_not_reported = self::isDebtor($client->id);
            $link = Url::to(['/report'], true);

            if ($app_ids_not_reported) {
                $app_info = [];
                $apparatuses = ApparatusToClient::find()->andWhere(['IN', 'id', $app_ids_not_reported])->all() ?? null;

                if (!$apparatuses) {
                    Yii::info('Client: ' . $client->official_name, 'test');
                    Yii::info('Apparatuses not found', 'test');
                }

                Yii::info($apparatuses, 'test');

                /** @var ApparatusToClient $atc */
                foreach ($apparatuses as $atc) {
                    Yii::info($atc, 'test');
                    if ($atc->apparatus) {
                        $app_info[] = '<li>' . $atc->apparatus->getApparatusFullName() . ' №' . $atc->serial_number . '</li>';
                    }
                }
                Yii::info($app_info, 'test');

                if (!$app_info) {
                    break;
                }

                $app_str = implode('', $app_info);

                /** @var EmailHelper $email */
                $email = new EmailHelper();
                $email->email_type = EmailHelper::TYPE_CLIENT;
                $email->sender_name = 'Компания ДЖЕСТ';
                $email->recipients = $client->email;
                $email->subject = 'Важно! (отвечать на это письмо не нужно)';

                $pcl = Html::a($link, $link); //Personal Cabinet Link
                $mail_to = Html::a('service@kcepokc.ru', ['mailto:service@kcepokc.ru']);

                $email->html_body = <<<HTML
<div><b>Добрый день, $client->official_name!</b></div>
<br>
Просим Вас предоставить текущие показания счетчиков следующих печатных устройств, находящихся на покопийном контракте:<br>
<ul>
$app_str
</ul>
<br>
Сделать это Вы можете в своем «Личном кабинете» $pcl. 
<br><br>
Если Вы не знаете, как посмотреть счетчики в аппаратах или Вы забыли свой логин и пароль, просьба написать запрос на $mail_to с указанием номера Вашего договора или названия организации.
<br><br>
Хорошего дня!
<br>
Сервисный центр,
<br> 
<b>Компания ДЖЕСТ.</b>
HTML;
                if ($email->send()){
                    $ctn = new ClientToNotify();
                    $ctn->client_id = $client->id;
                    if (!$ctn->save()){
                        Yii::error($ctn->errors, '_error');
                        return false;
                    }
                }
            }
        }
        return true;
    }


}
