<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;

/**
 * ReportSearch represents the model behind the search form about `app\models\Report`.
 */
class ReportSearch extends Report
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'apparatus_to_client_id', 'counter_1', 'counter_2', 'status'], 'integer'],
            [['created_at', 'sented_at', 'photo', 'report_month', 'client_name', 'apparatus_full_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find();

        $query->joinWith(['client']);
        $query->joinWith(['apparatus']);
        $query->joinWith(['brand']);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
//                    'apparatus_full_name',
//                    'counter_1',
//                    'counter_2',
//                    'photo',
//                    'sented_at',
                    'id'
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'sented_at' => $this->sented_at,
            'apparatus_to_client_id' => $this->apparatus_to_client_id,
            'counter_1' => $this->counter_1,
            'counter_2' => $this->counter_2,
            'report_month' => $this->report_month,
            'report_year' => $this->report_year,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'photo', $this->photo]);
        $query->andFilterWhere(['like', 'client.official_name', $this->client_name]);
        $query->orFilterWhere(['like', 'apparatus_to_client.serial_number', $this->apparatus_full_name]);
        $query->orFilterWhere(['like', 'brand.name', $this->apparatus_full_name]);
        $query->orFilterWhere(['like', 'apparatus.model', $this->apparatus_full_name]);

        return $dataProvider;
    }
}
