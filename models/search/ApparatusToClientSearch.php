<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ApparatusToClient;

/**
 * ApparatusToClientSearch represents the model behind the search form about `app\models\ApparatusToClient`.
 */
class ApparatusToClientSearch extends ApparatusToClient
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'apparatus_id', 'client_id', 'is_owner'], 'integer'],
            [['notes', 'serial_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ApparatusToClient::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'apparatus_id' => $this->apparatus_id,
            'client_id' => $this->client_id,
            'is_owner' => $this->is_owner,
        ]);

        $query->andFilterWhere(['like', 'notes', $this->notes])
            ->andFilterWhere(['like', 'serial_number', $this->serial_number]);

        return $dataProvider;
    }
}
