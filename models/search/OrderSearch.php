<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * OrderSearch represents the model behind the search form about `app\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'id',
                    'client_id',
                    'counter_1',
                    'counter_2',
                    'counter_3',
                    'status',
                    'apparatus_client_id',
                    'service_type',
                    'work_type',
                    'parent_order_id',
                    'total_time',
                    'manager_id',
                    'engineer_id',
                    'is_repeat',
                    'search_type_date',
                ],
                'integer'
            ],
            [
                [
                    'created_at',
                    'photos',
                    'done_at',
                    'account_number',
                    'serial_number',
                    'client_name',
                    'apparatus_name',
                    'search_start_date',
                    'search_end_date',
                    'engineer_fio',
                ],
                'safe'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();
        $query->joinWith(['brand']);
        $query->joinWith(['client']);
        $query->joinWith(['engineer']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $dataProvider->setSort([
            'attributes' => [
                'id',
                'client_name',
                'apparatus_name',
                'serial_number',
                'service_type',
                'work_type',
                'account_number',
                'done_at',
                'total_time',
                'engineer_id',
                'created_at',
                'engineer_fio' => [
                    'asc' => ['users.fio' => SORT_ASC],
                    'desc' => ['users.fio' => SORT_DESC],
//                    'default' => SORT_DESC,
                    'label' => 'Инженер',
                ],
            ]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'created_at' => $this->created_at,
            'client_id' => $this->client_id,
            'counter_1' => $this->counter_1,
            'counter_2' => $this->counter_2,
            'counter_3' => $this->counter_3,
            'apparatus_client_id' => $this->apparatus_client_id,
            'service_type' => $this->service_type,
            'work_type' => $this->work_type,
            'parent_order_id' => $this->parent_order_id,
            'total_time' => $this->total_time,
            'manager_id' => $this->manager_id,
            'engineer_id' => $this->engineer_id,
            'is_repeat' => $this->is_repeat,
        ]);

        $query->andFilterWhere(['like', 'photos', $this->photos])
            ->andFilterWhere(['like', 'account_number', $this->account_number]);

        $query->andFilterWhere(['LIKE', 'done_at', $this->done_at]);

        $query->andFilterWhere(['LIKE', 'apparatus_to_client.serial_number', $this->serial_number]);
        $query->andFilterWhere(['LIKE', 'client.official_name', $this->client_name]);

        $query->andFilterWhere([
            'OR',
            ['LIKE', 'apparatus.model', $this->apparatus_name],
            ['LIKE', 'brand.name', $this->apparatus_name]
        ]);

        $query->andFilterWhere(['LIKE', 'users.fio', $this->engineer_fio]);


        if ($this->status == Order::STATUS_CURRENT) {
            $query->andFilterWhere(['OR', ['status' => Order::STATUS_NEW], ['status' => Order::STATUS_WORK]]);
        } else {
            $query->andFilterWhere(['status' => $this->status]);
        }

        if ($this->status == Order::STATUS_DONE) {
            $query->orderBy(['done_at' => SORT_DESC]);
        }

        if ($this->search_start_date && !$this->search_end_date) {
            $this->search_end_date = date('Y-m-d', time());
        }

        if ($this->search_type_date == 1) {
            //Поиск по дате исполнения заявки
            $query->andFilterWhere(['BETWEEN', 'done_at', $this->search_start_date, $this->search_end_date]);
        } elseif ($this->search_type_date == 0) {
            //Поиск по дате создания заявки
            $query->andFilterWhere(['BETWEEN', 'created_at', $this->search_start_date, $this->search_end_date]);
        }

        return $dataProvider;
    }
}
