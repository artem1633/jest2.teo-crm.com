<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "consumables".
 *
 * @property int $id
 * @property string $name Наименование расходника
 * @property int $apparatus_type Тип аппарата для которого предназначен расходник
 *
 * @property OrderToConsumables[] $orderToConsumables
 */
class Consumables extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'consumables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['apparatus_type'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['name', 'apparatus_type'], 'required'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование расходника',
            'apparatus_type' => 'Тип аппарата для которого предназначен расходник',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderToConsumables()
    {
        return $this->hasMany(OrderToConsumables::className(), ['consumables_id' => 'id']);
    }

    public function getList()
    {
        return ArrayHelper::map(Consumables::find()->all(), 'id', 'name');
    }
}
