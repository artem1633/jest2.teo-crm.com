<?php

namespace app\models;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;


/**
 * This is the model class for table "brand".
 *
 * @property int $id
 * @property string $name Наименование
 * @property array $list Список марок для выпадающих списков
 *
 * @property Apparatus[] $apparatuses
 */
class Brand extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApparatuses()
    {
        return $this->hasMany(Apparatus::className(), ['brand_id' => 'id']);
    }

    public function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
