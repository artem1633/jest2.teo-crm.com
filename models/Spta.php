<?php

namespace app\models;

use app\models\query\SptaQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "spta".
 *
 * @property int $id
 * @property int $order_id Заявка
 * @property string $created_at
 * @property int $type Тип Установленные/Необходимые
 * @property string $name Название
 * @property string $num Кол-во
 *
 * @property Order $order
 * @property string $typeLabel
 */
class Spta extends ActiveRecord
{

    const TYPE_INSTALL = 1;
    const TYPE_NEED = 2;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'spta';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'type'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            ['num', 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заявка',
            'created_at' => 'Created At',
            'type' => 'Тип',
            'name' => 'Название',
            'num' => 'Кол-во',
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert){
            if (!$this->created_at){
                $this->created_at = date('Y-m-d H:i:s', time());
            }
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return SptaQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new SptaQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function getTypeList()
    {
        return [
            self::TYPE_INSTALL => 'Установленные',
            self::TYPE_NEED => 'Необходимые',
        ];
    }

    /**
     * Возвращает название статуса
     * @return mixed|null
     */
    public function getTypeLabel()
    {
        if ($this->type){
            return $this->getTypeList()[$this->type];
        }

        return null;
    }
}
