<?php

namespace app\models;

use yii\db\ActiveRecord;


/**
 * This is the model class for table "work".
 *
 * @property int $id
 * @property int $order_id Заявка
 * @property int $status Статус Выполненные/Необходимые
 * @property string $content Содержание работ
 * @property string $created_at
 *
 * @property Order $order
 * @property string $statusLabel
 */
class Work extends ActiveRecord
{
    const STATUS_FINISH = 1;
    const STATUS_NEED = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'status'], 'integer'],
            [['content'], 'string'],
            [['created_at'], 'safe'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заявка',
            'status' => 'Статус',
            'content' => 'Содержание работ',
            'created_at' => 'Created At',
        ];
    }

    public function beforeSave($insert)
    {
        if (!$this->created_at){
            $this->created_at = date('Y-m-d H:i:s', time());
        }
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return \app\models\query\WorkQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \app\models\query\WorkQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function getStatusList()
    {
        return [
            self::STATUS_FINISH => 'Выполненные',
            self::STATUS_NEED => 'Необходимые',
        ];
    }

    /**
     * Возвращает название статуса
     * @return mixed|null
     */
    public function getStatusLabel()
    {
        if ($this->status){
            return $this->getStatusList()[$this->status];
        }

        return null;
    }
}
