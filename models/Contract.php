<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;

/**
 * This is the model class for table "contract".
 *
 * @property int $id
 * @property int $client_id Клиент
 * @property string $number Номер
 * @property string $date Дата
 * @property string $file Файл контракта
 *
 * @property Client $client
 */
class Contract extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contract';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id'], 'integer'],
            [['date'], 'safe'],
            [['number'], 'string', 'max' => 255],
            [
                ['client_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Client::className(),
                'targetAttribute' => ['client_id' => 'id']
            ],
            ['file', 'file'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Клиент',
            'number' => 'Номер',
            'date' => 'Дата',
            'file' => 'Файл договора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Client::className(), ['id' => 'client_id']);
    }

    /**
     * Загрузка файла с реквизитами
     */
    public function uploadBankDetails()
    {
        $form = new UploadForm();
        $form->scenario = $form::SCENARIO_DOCUMENT;

        $form->file = UploadedFile::getInstance($this, 'file');

        if (!$form->file) {
            return false;
        }

        $tmp_name = 'contract';
        $target_dir = 'uploads/client/' . $this->id;

        if ($file_path = $form->upload($target_dir, $tmp_name, true)) {
            //Файл успешно загружен
            $this->file = $file_path;
            if (!$this->save()) {
                Yii::error($this->errors, '_error');
            } else {
                return true;
            }
        } else {
            Yii::error($form->errors, '_error');
        }
        return false;
    }
}
