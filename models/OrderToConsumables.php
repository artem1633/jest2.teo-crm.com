<?php

namespace app\models;

use app\models\query\OrderToConsumablesQuery;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "order_to_consumables".
 *
 * @property int $id
 * @property int $order_id Заказ
 * @property int $consumables_id Расходники
 * @property double $number Кол-во
 * @property array $consumable_list Расходники в форме добавления
 *
 * @property Consumables $consumables
 * @property Order $order
 */
class OrderToConsumables extends ActiveRecord
{
    public $consumable_list;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_to_consumables';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'consumables_id'], 'integer'],
            [['number'], 'number'],
            [['consumables_id'], 'exist', 'skipOnError' => true, 'targetClass' => Consumables::className(), 'targetAttribute' => ['consumables_id' => 'id']],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            ['consumable_list', 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Заказ',
            'consumables_id' => 'Расходник',
            'consumable_list' => 'Расходник',
            'number' => 'Кол-во',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsumables()
    {
        return $this->hasMany(Consumables::className(), ['id' => 'consumables_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * {@inheritdoc}
     * @return OrderToConsumablesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderToConsumablesQuery(get_called_class());
    }

    /**
     * Сохраняет расходник и кол-во
     * @return bool
     * @throws \yii\db\Exception
     */
    public function saveConsumables()
    {
        \Yii::info($this->attributes, 'test');

        //Очищаем все расходники для заказа
        OrderToConsumables::deleteAll(['order_id' => $this->order_id]);

        $rows = [];
        foreach ($this->consumables_id as $key => $cons_id) {
            if (!$cons_id || !$this->number[$key]) continue; //Пропускаем если не выбран расходник или не указано кол-во

            $rows[] = [$this->order_id, $cons_id, $this->number[$key]];
        }

        \Yii::$app->db->createCommand()->batchInsert(self::tableName(), ['order_id', 'consumables_id', 'number'], $rows)->execute();
        return true;
    }
}
