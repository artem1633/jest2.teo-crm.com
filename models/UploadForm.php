<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * @property UploadedFile $file
 * @property UploadedFile[] $files
 */
class UploadForm extends Model
{
    const SCENARIO_IMAGE = 'image';
    const SCENARIO_DOCUMENT = 'document';
    const SCENARIO_SCAN = 'scan';

    public $file;
    public $files;

    public function rules()
    {
        return [
            ['files', 'safe'],
            [
                ['file'],
                'file',
                'skipOnEmpty' => false,
                'extensions' => ['jpg', 'jpeg', 'png', 'gif'],
                'on' => $this::SCENARIO_IMAGE
            ],
            [
                ['file'],
                'file',
                'skipOnEmpty' => false,
                'extensions' => ['doc', 'docx', 'odt', 'txt', 'pdf'],
                'on' => $this::SCENARIO_DOCUMENT
            ],
            [
                ['file'],
                'file',
                'skipOnEmpty' => false,
                'extensions' => ['jpg', 'jpeg', 'tiff', 'pdf'],
                'on' => $this::SCENARIO_SCAN
            ],
            ['files', 'safe'],
        ];
    }

    /**
     * @param string $target_dir Целевая папка
     * @param string $tmp_name имя файла (без расширения)
     * @param bool $clean_dir Очищать или нет $target_dir
     * @return bool|string
     */
    public function upload($target_dir, $tmp_name, $clean_dir = false)
    {
        Yii::info('Target dir: ' . $target_dir, 'test');

        if ($clean_dir) {
            try {
                $files = array_diff(scandir('/' . $target_dir), ['..', '.']);
                Yii::info($files, 'test');
                foreach ($files as $file) {
                    $file_path = $target_dir . '/' . $file;

                    if (is_file($file_path)) {
                        try {
                            unlink($file_path);
                        } catch (\Exception $e) {
                            Yii::error($e->getMessage(), '_error');
                        }
                    }
                }
            } catch (\Exception $e) {
                Yii::error($e->getMessage(), '_error');
            }
        }
        if (!is_dir($target_dir)) {
            mkdir($target_dir, 0777, true);
        }

        if ($this->file) {
            if ($this->validate('file')) {
                $file_path = $target_dir . '/' . $tmp_name . '.' . $this->file->extension;
                $this->file->saveAs($file_path);
                return $file_path;
            } else {
                Yii::error($this->errors, '_error');
                return false;
            }

        } elseif ($this->files) {
            if ($this->validate('files')) {
                $files = [];
                $counter = 1;
                foreach ($this->files as $file) {
                    //К имени файла добавляем значение $counter т.к. для обного счетчика может быть загружено больше одной фото
                    $file_path = $target_dir . '/' . $tmp_name . '_' . $counter . '.' . $file->extension;
                    $file->saveAs($file_path);
                    $files[$counter][] = $file_path;
                    $counter++;
                }
                return $files;
            } else {
                Yii::error($this->errors, '_error');
                return false;
            }
        } else {
            Yii::error('Файл/ы не загружены', '_error');
            return false;
        }
    }
}